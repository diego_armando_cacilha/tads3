package TadsIII.EstruturaDeDados.Arvores;



public class Arvore {

    private Noh raiz;


    public Arvore () {

        raiz = null;
    }
    
    /**
     * Imprime o retorno do método emOrdem().
     */
    public void imprimirEmOrdem(){
        emOrdem(raiz);
    }
    
    /**
     * Imprime ERD (Esquerda, Raiz, Direita)
     * @param tmp Noh
     */
    private void emOrdem (Noh tmp){
        if(tmp != null){
            emOrdem(tmp.getEsquerda());
            System.out.println("Chave: " + tmp.getChave());
            emOrdem(tmp.getDireita());
        }
    }
    
    /**
     * Imprime o retorno do método preOrdem().
     */
    public void imprimirPreOrdem(){
        preOrdem(raiz);
    }
    
    /**
     * Imprime RED (Raiz, Esquerda, Direita)
     * @param tmp Noh
     */
    private void preOrdem(Noh tmp){
        if(tmp != null){
            System.out.println("Chave: " + tmp.getChave());
            System.out.println("Esquerda: " + tmp.getEsquerda().getChave());
            System.out.println("Direita: " + tmp.getDireita().getChave());
        }
    }


    public boolean arvoreVazia () {

        return raiz == null;
    }


    private Noh encontrarLugar (int chave) {

        Noh tmp = raiz, pai = null;

        if (!arvoreVazia()) {

            while (tmp != null) {
                pai = tmp;

                if (chave >= tmp.getChave())
                    tmp = tmp.getDireita();
                else
                    tmp = tmp.getEsquerda();
            }

            return pai;
        }
        else
            return null;
    }


    private Noh buscarElemento (int chave) {

        Noh tmp = raiz, pai = null;

        if (!arvoreVazia()) {

            while (tmp != null) {
                if (chave != tmp.getChave()) {
                    pai = tmp;

                    if (chave >= tmp.getChave())
                        tmp = tmp.getDireita();
                    else
                        tmp = tmp.getEsquerda();
                }
                else
                    break;
            }

            return pai;
        }
        else
            return null;
    }


    private Noh atravessarArvore (Noh ant, Noh tmp, boolean esq) {

        if (!arvoreVazia()) {

            while (tmp != null) {
                ant = tmp;

                if (esq == true)
                    tmp = tmp.getEsquerda();
                else
                    tmp = tmp.getDireita();
            }

            return ant;
        }
        else
            return null;
    }


    public void inserirElemento (int valor) {

        Noh novo, ant;

        novo = new Noh(valor);

        if (!arvoreVazia()) {
            ant = encontrarLugar(valor);

            if (valor >= ant.getChave())
                ant.setDireita(novo);
            else
                ant.setEsquerda(novo);
        }
        else
            raiz = novo;
    }


    public void removerElemento (int valor) {

        Noh tmp = null, aux = null, pai =  null;

        if (!arvoreVazia()) {
            pai = buscarElemento(valor);

            if (valor > pai.getChave()) {
                aux = pai.getDireita();

                if (aux.getEsquerda() != null) {
                    pai.setDireita(aux.getEsquerda());
                    tmp = atravessarArvore(aux.getEsquerda(), aux.getEsquerda().getDireita(), false);
                    tmp.setDireita(aux.getDireita());
                }
                else
                    pai.setDireita(aux.getDireita());
            }
            else {
                aux = pai.getEsquerda();

                if (aux.getDireita() != null) {
                    pai.setEsquerda(aux.getDireita());
                    tmp = atravessarArvore(aux.getDireita(), aux.getDireita().getEsquerda(), true);
                    tmp.setEsquerda(aux.getEsquerda());
                }
                else
                    pai.setEsquerda(aux.getEsquerda());
            }
        }
    }

}
