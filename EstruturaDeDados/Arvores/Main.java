/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TadsIII.EstruturaDeDados.Arvores;

/**
 *
 * @author aluno
 */
public class Main {
    public static void main(String[] args) {
        Arvore ar = new Arvore();
        
        ar.inserirElemento(15);
        ar.inserirElemento(11);
        ar.inserirElemento(55);
        
        //ar.imprimirPreOrdem();
        
        ar.imprimirEmOrdem();
    }
    
}
