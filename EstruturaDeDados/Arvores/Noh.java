/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TadsIII.EstruturaDeDados.Arvores;

class Noh {

    private int chave;
    private Noh esquerda;
    private Noh direita;


    public Noh (int chave) {

        this.chave = chave;
        this.esquerda = null;
        this.direita = null;
    }


    public void setEsquerda (Noh esq) {

        this.esquerda = esq;
    }


    public void setDireita (Noh dir) {

        this.direita = dir;
    }


    public int getChave () {

        return chave;
    }


    public Noh getEsquerda () {

        return esquerda;
    }


    public Noh getDireita () {

        return direita;
    }
}
