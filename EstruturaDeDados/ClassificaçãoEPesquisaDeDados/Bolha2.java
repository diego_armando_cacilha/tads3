/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EstruturaDeDados.ClassificaçãoEPesquisaDeDados;

import static EstruturaDeDados.ClassificaçãoEPesquisaDeDados.QuickSort.quickSort;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author Diego Armando Cacilha
 */
public class Bolha2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Informe o tamanho do array: ");
        
        int tam = scanner.nextInt();
        int array1[] = new int[tam];
        int array2[] = new int[tam];
        
        Random random = new Random(System.currentTimeMillis());
        
        for(int x = 0; x < tam; x++)
        {
            int tmp = random.nextInt() % 1000;
            tmp = (tmp < 0) ? tmp*(-1) : tmp;
            
            array1[x] = tmp;
            array2[x] = tmp;
            
            System.out.println("Valor: "+array1[x]);
        }
        
        System.out.println("Ordenado");
        quickSort(array1, 0, array1.length - 1);
        
        for(int y = 0; y<tam; y++)
        {
            System.out.println("Valor: "+array1[y]);
        }
    }
    
}
