/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EstruturaDeDados.ClassificaçãoEPesquisaDeDados.MergeSort;

/**
 *
 * @author guest-lUFA8R
 */
public class MergeSort {

    public static void merge(int array[], int esq, int meio, int dir)
    {
        //Verifica os tamanhos dos subarrays a serem fundidos
        int n1 = meio - esq + 1;
        int n2 = dir - meio;
        
        //Cria a inicialização dos arrays temporarios
        int e[] = new int[n1];
        int d[] = new int[n2];
        
        //copia os dados para os arrays temporários
        for (int i = 0; i<n1;++i)
            e[i] = array[esq + i];
        
        for (int j = 0; j<n2;++j)
            d[j] = array[meio + 1 + j];
        
        //Fusão dos arrays temporários 
        //Indices de início do primeiro de segundo subarray
        int i = 0, j = 0;
        
        //Índice do início do subarray a ser fundido
        int k = esq;
        while(i < n1 && j < n2)
        {
            if(e[i] <= d[j])
            {
                array[k] = e[i];
                i++;
            }else
            {
                array[k] = d[j];
                j++;
            }
            k++;
        }
        
        //Copia os elementos restantes do array e[], se houver
        while(i < n1)
        {
            array[k] = e[i];
            i++;
            k++;
        }
        
        //Copia os elementos restantes do array d[], se houver
        while(j < n2)
        {
            array[k] = d[j];
            j++;
            k++;
        }

    }
    
    public static void sort (int array[], int esq, int dir)
    {
        if(esq < dir)
        {
            //Procura o meio do array para dividí-lo
            int meio = (esq + dir) /2;
            
            //Ordena a primeira e segunda metade
            sort(array, esq, meio);
            sort(array, meio + 1, dir);
            
            //Fusão das metades ordenadas
            merge(array, esq, meio, dir);
        }
    }
    
}
