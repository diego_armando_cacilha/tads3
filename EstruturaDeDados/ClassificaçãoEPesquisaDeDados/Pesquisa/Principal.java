/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EstruturaDeDados.ClassificaçãoEPesquisaDeDados.Pesquisa;

/**
 *
 * @author Diego Armando Cacilha
 */
public class Principal {
    /**
     * 
     * @param array
     * @param key
     * @return indice do array, caso valor não for encontrado, retorna -1 (-1 não existe em arrays)
     * 
     * @descrição Pesquisa linear: varre o array procurando o valor informado como parâmetro
     */
    public static int pesquisa (int array[], int key)
    {
        for(int i = 0; i < array.length; i++)
        {
            if(key == array[i])
            {
                return i;
            }
        }
        return -1;
    }
    
    /**
     * 
     * @param key
     * @param array
     * @return true or false
     * 
     * @descrição pesquisa um valor dentro do array dividindo-o sempre pela metade até encontrar o valor
     */
    public static boolean binaria(int key, int array[])
    {
        int meio;
        int inicio = 0;
        int fim = array.length-1;
        
        while(inicio <= fim)
        {
            meio = (inicio + fim) / 2;
            
            if(key == array[meio])
            {
                return true;
            }
            
            if(key < array[meio])
            {
                fim = meio - 1;
            }else
            {
                inicio = meio + 1;
            }
        }
        return false;
    }
    
    public static void main(String[] args) {
        int[] meuArray = {5, 9, 12, 14, 18, 22, 23, 25, 26, 40};
        int a = 24;
        
        System.out.println(binaria(a, meuArray));
        //System.out.println(pesquisa(meuArray, a));
        
    }
    
}
