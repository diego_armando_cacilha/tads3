/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EstruturaDeDados.ClassificaçãoEPesquisaDeDados;

/**
 *
 * @author aluno
 */
public class QuickSort {
    public static int separacao(int array[], int esq, int dir)
        {
            int i = esq;
            int j = dir;
            int tmp;
            int pivo = array[(esq + dir)/2];

            while(i <= j)
            {
                while(array[i] < pivo)
                    i++;
                while(array[j] > pivo)
                    j--;
                if(i <= j)
                {
                    tmp = array[i];
                    array[i] = array[j];
                    array[j] = tmp;
                    i++;
                    j--;
                }
            }
            return i;
        }

        public static void quickSort(int array[], int esq, int dir)
        {
            if(esq < dir)
            {
                int ind = separacao(array, esq, dir);
                quickSort(array, esq, ind -1);
                quickSort(array, ind, dir);
            }
        }
}