/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EstruturaDeDados.Exercicios.ListaDuplamenteEncadeada._01;

import EstruturaDeDados.ListaDuplamenteEncadeada._01.Nodo;

/**
 *
 * @author aluno
 */
public class ListaDuplamenteEncadeada {
    
    Nodo inicio, fim;
    int tamanho;

    public ListaDuplamenteEncadeada () {

        tamanho = 0;
        inicio = null;
        fim = null;
    }
    
    public void inserirOrdenado(int valor){
        Nodo tmp = null;
        if(listaVazia()){
            inserirInicio(valor);
        }else{
            int aux = this.tamanho;
            for(int i = 1;i <= aux;i++){
                tmp = retornaPosicao(i);
                
                if(tmp.getDado() < valor){
                    inserirPosicao(i, valor);
                }else{
                    inserirPosicao(i++, valor);
                }
                
            }
            
            
//            for(int i = 1; i <= this.tamanho;i++){
//                if (inicio.getDado()<valor){
//                    
//                }
//                int aux = retornaPosicao(i).getDado();
//                
//            }

        }
    }


    public boolean listaVazia () {

        return tamanho == 0;
    }

    public void inserirInicio (int elemento) {

        Nodo novo = new Nodo(elemento);

        if (!listaVazia()) {
            inicio.setAnterior(novo);
            novo.setProximo(inicio);

            inicio = novo;
        }
        else {
            inicio = novo;
            fim = novo;
        }

        tamanho++;
    }


    public void inserirFinal (int elemento) {

        Nodo novo = new Nodo(elemento);

        if (!listaVazia()) {
            novo.setAnterior(fim);
            fim.setProximo(novo);
            fim = novo;
        }
        else {
            inicio = novo;
            fim = novo;
        }

        tamanho++;
    }


    public void inserirPosicao (int posicao, int elemento) {

        if (posicao == 1)
            inserirInicio(elemento);
        else if (posicao == tamanho + 1)
            inserirFinal(elemento);
        else if (posicao <= tamanho) {
            Nodo novo = new Nodo(elemento);
            Nodo tmp = retornaPosicao(posicao);

            novo.setAnterior(tmp.getAnterior());
            tmp.setAnterior(novo);

            novo.setProximo(tmp);
            novo.getAnterior().setProximo(novo);

            tamanho++;
        }
    }


    public int removerInicio () {

        if (!listaVazia()) {
            int dado = inicio.getDado();

            inicio = inicio.getProximo();

            if (inicio != null)
                inicio.setAnterior(null);
            else
                fim = inicio;

            tamanho--;

            return dado;
        }

        return -1;
    }


    public int removerFinal () {

        if (!listaVazia()) {
            int dado = fim.getDado();

            fim = fim.getAnterior();

            if (fim != null)
                fim.setProximo(null);
            else
                inicio = fim;

            tamanho--;

            return dado;
        }

        return -1;
    }


    public void removerPosicao (int posicao) {

        if (posicao == 1)
            removerInicio();
        else if (posicao == tamanho)
            removerFinal();
        else if (posicao <= tamanho) {
            Nodo tmp = retornaPosicao(posicao);

            tmp.getAnterior().setProximo(tmp.getProximo());
            tmp.getProximo().setAnterior(tmp.getAnterior());

            tamanho--;
        }
    }


    private Nodo retornaPosicao (int posicao) {

        Nodo tmp = null;
        int indice = 1;

        if (posicao >= ((tamanho / 2) + 1)) {
            tmp = fim;

            while ((tmp.getAnterior() != null) && (indice <= (tamanho - posicao))) {
                tmp =  tmp.getAnterior();
                indice++;
            }
        }
        else {
            tmp = inicio;

            while ((tmp.getProximo() != null) && (indice < posicao)) {
                tmp = tmp.getProximo();
                indice++;
            }
        }

        return tmp;
    }


    public void imprimeLista () {

        if (!listaVazia()) {
            int i = 1;
            Nodo tmp = inicio;

            while (tmp != null) {
                System.out.println(i + "o. elemento: " + tmp.getDado());
                tmp = tmp.getProximo();
                i++;
            }
        }
    }


    //public void inserirOrdenado (int elemento)


    //public void inverterLista ()
}

