package EstruturaDeDados.Exercicios.Pilha;

/**
 *
 * @author Diego
 */
public class MinhaPilha{

    private Caixa[] elementos;
    private int topo;
    
    /**
     * Classe que insere elementos num array de inteiros
     * 
     * @param elemento int que será inserido na pilha.
     * @throws Exception Stack Overflow
     */
    public void push(Caixa elemento) throws Exception{
        if (!pilhaCheia())
            elementos[++topo] = elemento;
        else
            throw new Exception("Stack Overflow");
    }
    
    public Caixa pop() throws Exception{
        
        if(!pilhaVazia())
            return elementos[topo--];
        else
            throw new Exception("Stack Underflow!");
    }
    
    public Caixa top() throws Exception{
        if(!pilhaVazia())
            return elementos[topo];
        else
            throw new Exception("Stack Underflow");
    }

    public Caixa imprime(){
        for(int i = 0; i< elementos.length;i++){
            return elementos[1];
        }
        return null;
    }

    public MinhaPilha(int tamanho){
        this.elementos = new Caixa[tamanho];
        this.topo = -1;
    }
    
    public boolean pilhaCheia(){
        return topo == (elementos.length-1);
    }
    
    public boolean pilhaVazia(){
        return topo == -1;
    }

    public int getTopo() {
        return topo;
    }
    
    

    
}
