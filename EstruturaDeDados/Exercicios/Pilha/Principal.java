/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EstruturaDeDados.Exercicios.Pilha;

/**
 *
 * @author Diego
 */
public class Principal {
    public static void main(String[] args) {
        Caixa cai = new Caixa(20);
        Caixa cai2 = new Caixa(2);
        Caixa cai3 = new Caixa(23);
        Caixa cai4 = new Caixa(12);
        Caixa cai5 = new Caixa(10);
        Caixa cai6 = new Caixa(16);
        
        MinhaPilha pilha = new MinhaPilha(5);
        
        
        try {
            pilha.push(cai);
            pilha.push(cai2);
            pilha.push(cai3);
            pilha.push(cai4);
            pilha.push(cai5);
        } catch (Exception e) {
            System.out.println(e);
        }
        System.out.println(pilha.imprime());
        

    }
    
}
