/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TadsIII.EstruturaDeDados.Fila.Exercicio;

import EstruturaDeDados.Fila.Fila;
import EstruturaDeDados.Pilhas.Pilha;

/**
 *
 * @author aluno-informatica
 */
public class InverteFila {
    
    public InverteFila(Fila fil){
        Pilha minhaPilha = new Pilha(5);
        try {
        
            while(!fil.filaVazia()){
                minhaPilha.push(fil.dequeue());
            }
            while(!minhaPilha.pilhaVazia()){
                fil.enqueue(minhaPilha.pop());
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
}
