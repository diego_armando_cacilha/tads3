/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TadsIII.EstruturaDeDados.Fila.Exercicio;

import EstruturaDeDados.Fila.Fila;
import EstruturaDeDados.Pilhas.Pilha;

/**
 *
 * @author aluno-informatica
 */
public class Principal {
    public static void main(String[] args) {
        Fila minhaFila1 = new Fila(5);
        Fila minhaFila2 = new Fila(5);
        
        
        try {
            minhaFila2.enqueue(3);
            minhaFila2.enqueue(4);
            minhaFila2.enqueue(7);
            minhaFila2.enqueue(9);
            minhaFila2.enqueue(11);
            
            minhaFila1.enqueue(10);
            minhaFila1.enqueue(8);
            minhaFila1.enqueue(6);
            minhaFila1.enqueue(5);
            minhaFila1.enqueue(2);
        } catch (Exception e) {
            System.out.println(e);
        }
        InverteFila infFil = new InverteFila(minhaFila1);
//        try {
//            System.out.println(minhaFila1.first());
//        } catch (Exception e) {
//            System.out.println(e);
//        }
        
        UnirFilasCrescente unir = new UnirFilasCrescente(minhaFila1, minhaFila2, 10);
        try {
            System.out.println(unir.filaAux.first());
        } catch (Exception e) {
            System.out.println(e);
        }

        
    }
}
