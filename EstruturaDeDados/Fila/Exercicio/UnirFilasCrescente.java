/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TadsIII.EstruturaDeDados.Fila.Exercicio;

import EstruturaDeDados.Fila.Fila;

/**
 *
 * @author aluno-informatica
 */
public class UnirFilasCrescente {
    public Fila filaAux;
    
    public UnirFilasCrescente(Fila f1, Fila f2, int tamanho){
        filaAux = new Fila(tamanho);
        
        while(!f1.filaVazia() && !f2.filaVazia()){
            
            try {
                int aux1 = f1.first();
                int aux2 = f2.first();
                if(f1.first() < f2.first()){
                    filaAux.enqueue(f1.dequeue());
                }else{
                    filaAux.enqueue(f2.dequeue());
                }
            } catch (Exception e) {
                System.out.println(e);
            }
        }
            
    }
}
