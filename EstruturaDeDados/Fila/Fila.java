package EstruturaDeDados.Fila;

public class Fila {
    private int[] elementos;
    private int fim;
    private final int inicio;

    public Fila (int tamanho) {
        elementos = new int[tamanho];
        inicio = 0;
        fim = -1;
    }

    public void enqueue (int elemento) throws Exception {

        if (!filaCheia())
            elementos[++fim] = elemento;
        else
            throw new Exception("Fila Cheia!!!");
    }

    public int dequeue () throws Exception {

        if (!filaVazia()) {
            int dado = elementos[inicio];

            for (int i = 0; i<fim; i++)
                elementos[i] = elementos[i+1];

            fim--;

            return dado;
        }
        else
            throw new Exception("Fila Vazia!!!");
    }

    public int first () throws Exception {
        if (!filaVazia()){
            return elementos[inicio];
        }else {
            throw new Exception("Fila Vazia!!!");
        }
    }
    
    public boolean filaVazia () {
        return fim == -1;
    }

    public boolean filaCheia () {

        return fim == elementos.length-1;
    }
}