package EstruturaDeDados.Fila;

public class Principal {

    public static void main (String[] args) {

        try {
            Fila fila = new Fila(10);

            fila.enqueue(10);
            fila.enqueue(20);
            fila.enqueue(30);
            fila.enqueue(40);
            fila.enqueue(50);
            fila.enqueue(60);
            fila.enqueue(70);

            while (!fila.filaVazia())
                System.out.println("Elemento do Inicio: " + fila.dequeue());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}