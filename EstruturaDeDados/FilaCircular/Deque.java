/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EstruturaDeDados.FilaCircular;


public class Deque {

    private int[] elementos;
    private int inicioEsq, inicioDir, fimEsq, fimDir;


    public Deque (int tamanho) {

        elementos = new int[tamanho];
        inicioEsq = 0;
        inicioDir = tamanho - 1;
        fimEsq = -1;
        fimDir = tamanho;
    }


    public boolean esqVazia () {

        return inicioEsq > fimEsq;
    }


    public boolean dirVazia () {

        return inicioDir < fimDir;
    }


    public boolean dequeVazio () {

        return esqVazia() && dirVazia();
    }


    public boolean dequeCheio () {

        return fimDir - fimEsq == 1;
    }

    public void enqueueLeft (int elemento) {

        if (!dequeCheio())
            elementos[++fimEsq] = elemento;
    }


    public void enqueueRight (int elemento) {

        if (!dequeCheio())
            elementos[--fimDir] = elemento;
    }


    public int dequeueLeft () {

        if (!esqVazia()) {
            int dado = elementos[inicioEsq];

            for (int i = inicioEsq; i < fimEsq; i++)
                elementos[i] = elementos[i+1];

            fimEsq--;

            return dado;
        }

        return -1;
    }


    public int dequeueRight () {

        if (!dirVazia()) {
            int dado = elementos[inicioDir];

            for (int i = inicioDir; i > fimDir; i--)
                elementos[i] = elementos[i - 1];

            fimDir++;
        }

        return -1;
    }
}


