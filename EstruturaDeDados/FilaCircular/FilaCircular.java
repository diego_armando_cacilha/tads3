/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EstruturaDeDados.FilaCircular;


public class FilaCircular {

    private int[] elementos;
    private int inicio, fim, tamanho;

    public FilaCircular (int limite) {

        elementos = new int[limite];
        inicio = 0;
        fim = -1;
        tamanho = 0;

    }

    public boolean filaVazia () {

        return tamanho == 0;
    }


    public boolean filaCheia () {

        return tamanho == elementos.length;
    }


    public void enqueue (int elemento) throws Exception {

        if (!filaCheia()) {
            tamanho++;
            fim = ++fim % elementos.length;
            elementos[fim] = elemento;
        }
        else
            throw new Exception("Fila Cheia!!!");
    }


    public int dequeue () throws Exception {

        if (!filaVazia()) {
            int dado = elementos[inicio];
            inicio = ++inicio % elementos.length;
            tamanho--;

            return dado;
        }
        else
            throw new Exception("Fila Vazia!!!");
    }


    public int first () throws Exception {

        if (!filaVazia())
            return elementos[inicio];
        else
            throw new Exception("Fila Vazia!!!");

    }
}

