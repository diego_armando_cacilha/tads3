/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EstruturaDeDados.FilaCircular;

/**
 *
 * @author aluno
 */
public class Principal {
    public static void main(String[] args) {
        FilaCircular filCir = new FilaCircular(6);
        
        try {
            filCir.enqueue(2);
            filCir.enqueue(20);
            filCir.enqueue(12);
            filCir.enqueue(5);
            filCir.enqueue(3);
            filCir.enqueue(21);
            System.out.println(filCir.first());
            
        } catch (Exception e) {
            System.out.println(e);
        }
        
    }
    
}
