/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EstruturaDeDados.ListaDuplamenteEncadeada._01;

public class ListaDuplamenteEncadeada {
    
    Nodo inicio, fim;
    int tamanho;

    public ListaDuplamenteEncadeada () {

        tamanho = 0;
        inicio = null;
        fim = null;
    }
    
    public void inverteLista(){
        
    }


    public boolean listaVazia () {

        return tamanho == 0;
    }

    public void inserirInicio (int elemento) {

        Nodo novo = new Nodo(elemento);

        if (!listaVazia()) {
            inicio.setAnterior(novo);
            novo.setProximo(inicio);

            inicio = novo;
        }
        else {
            inicio = novo;
            fim = novo;
        }

        tamanho++;
    }


    public void inserirFinal (int elemento) {

        Nodo novo = new Nodo(elemento);

        if (!listaVazia()) {
            novo.setAnterior(fim);
            fim.setProximo(novo);
            fim = novo;
        }
        else {
            inicio = novo;
            fim = novo;
        }

        tamanho++;
    }

   // implementar os metodos removerFinal e removerInicio

}