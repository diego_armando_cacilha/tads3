/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EstruturaDeDados.ListaDuplamenteEncadeada._01;

public class Nodo {

    Nodo anterior;
    int dado;
    Nodo proximo;

    public Nodo (int dado) {

        this.anterior = null;
        this.dado = dado;
        this.proximo = null;
    }

    public int getDado () {

        return dado;
    }

    public void setAnterior (Nodo nodo) {

        anterior = nodo.anterior;
    }


    public void setProximo (Nodo nodo) {
        proximo = nodo.proximo;
    }


    public Nodo getAnterior () {

        return anterior;
    }


    public Nodo getProximo () {

        return proximo;
    }
}