/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EstruturaDeDados.Listas;


public class ListaEncadeada {

    Noh inicio;
    int tamanho;

    public ListaEncadeada () {

        tamanho = 0;
        inicio = null;
    }


    public boolean listaVazia () {

        return tamanho == 0;
    }

    public void inserirInicio (int elemento) {

        Noh tmp = new Noh(elemento);

        if (!listaVazia()) {
            tmp.proximo = inicio;
            inicio = tmp;
        }
        else
            inicio = tmp;

        tamanho++;
    }


    public void inserirFinal (int elemento) {

        Noh novo = new Noh(elemento);

        if (!listaVazia()) {
            Noh tmp = inicio;

            while (tmp.proximo != null) {
                tmp = tmp.proximo;
            }

            tmp.proximo = novo;
        }
        else
            inicio = novo;

        tamanho++;

    }


    public void inserirPosicao (int posicao, int elemento) {

        if (posicao <= tamanho) {
            Noh novo = new Noh(elemento);
            Noh tmp = inicio;
            int indice = 1;

            while ((tmp.proximo != null) && (indice < posicao-1)) {
                tmp = tmp.proximo;
                indice++;
            }

            novo.proximo = tmp.proximo;
            tmp.proximo = novo;
        }
    }


    public void imprimeLista () {

        if (!listaVazia()) {
            Noh tmp = inicio;

            while (tmp != null) {
                System.out.println("Elemento: " + tmp.dado);
                tmp = tmp.proximo;
            }
        }
    }
    
    public void removerInicio(){
        if (!listaVazia()){
            Noh tmp = inicio; //necessario guardar a referencia apenas se fizer uso do dado
            inicio = inicio.proximo;
            
            tamanho--;
        }
    }
    
    public void removerFinal(){
        if(!listaVazia()){
            Noh ant = null, tmp = inicio;
            
            while (tmp.proximo != null){
                ant = tmp;
                tmp = tmp.proximo;
            }
            
            if (ant!= null){
                ant.proximo = null;
            }
            else{
                inicio = null;
            }
            
            tamanho--;
        }
    }
    
    public void removerPosicao(int posicao){
        if (!listaVazia()){
            if (posicao <= tamanho){
                
                if (posicao == 1)
                    removerInicio();
                else{
                    if (posicao == tamanho)
                        removerFinal();
                    else{
                        Noh ant = null, tmp = inicio;
                        int indice = 1;
                        
                        while ((tmp.proximo != null) && (indice < posicao)){
                            ant = tmp;
                            tmp = tmp.proximo;
                            indice++;
                        }
                        
                        ant.proximo = tmp.proximo;
                    }
                }
                    
            }
            
            tamanho--;
        }
    }
}
