package EstruturaDeDados.Listas;
/**
 *
 * @author Diego Armando Cacilha
 */
public class ListaSequencial {
    private int tamanho, indice;
    private int[] dados;
    private boolean vazia;
    
    public ListaSequencial(int tamanho)
    {
        this.vazia = true;
        this.indice = -1;
        this.tamanho = tamanho;
        this.dados = new int[tamanho];
    }
    
    public void imprimeLista()
    {
        for(int i = 0; i<=indice; i++){
            System.out.println("Elamento [" + (i+1) + "] = " + this.dados[i]);
        }
        
    }
    
    public void inserirInicio(int elemento) throws Exception
    {
        if(indice < tamanho - 1)
        {
            indice++;
            for (int i = indice; i > 0; i--)
            {
                dados[i] = dados[i - 1];
            }
            dados[0] = elemento;
            vazia = false;
        }else
        {
            throw new Exception("Lista cheia");
        }
    }
    
    public void inserirFinal(int elemento) throws Exception
    {
        if(this.indice<this.tamanho-1)
        {
            dados[++indice] = elemento;
            vazia = false;
        }else
        {
            throw new Exception("Lista cheia");
        }
    }
    
    public void inserirPosicao(int posicao, int elemento) throws Exception
    {
        if((this.indice < tamanho - 1) && (posicao <= indice))
        {
            this.indice++;
            
            for(int i = this.indice; i>(posicao - 1); i--)
            {
                dados[i] = dados[i-1];
            }
            
            dados[posicao-1] = elemento;
            vazia = false;
        }else
        {
                throw new Exception("Lista cheia");
        }
    }
    
    public int removerInicio() throws Exception
    {
         int dado = dados[0];
        for (int i = 0; i < indice; i++) {
            dados[i] = dados[i + 1];
        }
        indice--;
        if (indice == -1) {
            vazia = true;

            return dado;
        } else {
            throw new Exception("lista Cheia");
        }
    }
    
    public int removerFinal() throws Exception
    {
         if (vazia != true) {
            int dado = dados[indice];
            indice--;
            if (indice == -1) {
                vazia = true;
            }
            return dado;
        } else {
            throw new Exception("lista Cheia");
        }
        
    }
    
    public int removerPosicao(int posicao) throws Exception
    {
        if ((vazia != true) && (posicao <= indice)) {
            int dado = dados[posicao - 1];
            for (int i = posicao - 1; i < this.indice; i++) {
                dados[i] = dados[i + 1];
            }
            indice--;

            if (indice == -1) {
                vazia = true;
            }

            return dado;
        } else {
            throw new Exception("lista Cheia");

        }
            
    }
    
}
