/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EstruturaDeDados.Listas;

/**
 *
 * @author aluno-informatica
 */

public class Principal {

    public static void main (String args[]) {

        try {
            /*ListaSequencial lista = new ListaSequencial(20);

            lista.inserirFinal(10);
            lista.inserirInicio(25);
            lista.inserirFinal(30);
            lista.inserirInicio(-1);
            lista.inserirPosicao(3,45);

            lista.removerInicio();
            lista.removerFinal();
            lista.removerPosicao(2);

            lista.imprimeLista();*/

            ListaEncadeadaCircular lista = new ListaEncadeadaCircular();

            lista.inserirFinal(10);
            lista.inserirInicio(20);
            lista.inserirFinal(12);
//            lista.inserirPosicao(2, 25);
//            lista.inserirPosicao(3, 49);

            lista.removerPosicao(3);

            lista.imprimeLista();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}

