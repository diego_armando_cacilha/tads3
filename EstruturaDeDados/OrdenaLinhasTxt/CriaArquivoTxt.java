/*
 * Cria o arquivo TXT com números aleatórios
 */
package TadsIII.EstruturaDeDados.OrdenaLinhasTxt;

import java.util.Random;

/**
 *
 * @author Diego Armando Cacilha
 */
public class CriaArquivoTxt {
    
    public void geraArquivo(){
        
        Random random = new Random(System.currentTimeMillis());
        int tam = 100;
        int array1[] = new int[tam];
        
        for(int x = 0; x < tam; x++)
        {
            int tmp = random.nextInt() % 1000;
            tmp = (tmp < 0) ? tmp*(-1) : tmp;
            
            array1[x] = tmp;
            
            System.out.println("Valor: "+array1[x]);
        }
    }
    
}
