package EstruturaDeDados.PilhaEncadeada;

import EstruturaDeDados.Listas.Noh;

public class PilhaEncadeada {

    Noh base, topo;
    int tamanho;

    public PilhaEncadeada () {

        tamanho = 0;
        base = null;
        topo = null;
    }


    public boolean pilhaVazia () {

        return tamanho == 0;
    }


    /**
     * Insere elemento no topo
     * 
     * @param elemento tamanho da pilha 
     */
    public void push (int elemento) {

        Noh novo = new Noh(elemento);

        if (!pilhaVazia()) {
            topo.proximo = novo;
            topo = novo;
            
        }
        else {
            base = novo;
            topo = base;
        }

        tamanho++;
    }


    /**
     * retira o elemento do topo
     * 
     * @return dado tipo Noh
     * @throws Exception 
     */
    public int pop () throws Exception {

        if (!pilhaVazia()) {
            int dado = topo.getDado();

            Noh ant = null, tmp = base;

            while (tmp.proximo != null) {
                ant = tmp;
                tmp = tmp.proximo;
            }

            if (ant != null) {
                ant.proximo = null;
                topo = ant;
            }
            else {
                base = null;
                topo = null;
            }

            tamanho--;

            return dado;
        }
        else
            throw new Exception("Pilha Vazia!");
    }


    /**
     * Consulta o topo
     * 
     * @return dado tipo Noh
     * @throws Exception 
     */
    public int top() throws Exception {

        if (!pilhaVazia()) {
            int dado = topo.getDado();
            return dado;
        }
        else
            throw new Exception("Pilha Vazia!");
    }
}
