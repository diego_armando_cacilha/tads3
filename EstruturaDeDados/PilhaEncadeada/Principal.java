/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EstruturaDeDados.PilhaEncadeada;

/**
 *
 * @author aluno-informatica
 */
public class Principal {
    public static void main(String[] args) {
        
        PilhaEncadeada pil = new PilhaEncadeada();
        
        pil.push(5);
        pil.push(50);
        
        try {
            System.out.println(pil.top());
        } catch (Exception e) {
            System.out.println(e);
        }

    }
    
}
