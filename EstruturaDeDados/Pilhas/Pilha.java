/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EstruturaDeDados.Pilhas;

/**
 *
 * @author aluno
 */
public class Pilha {
    private int[] elementos;
    private int topo;
    
    public Pilha(int tamanho){
        this.elementos = new int[tamanho];
        this.topo = -1;
    }
    
    public boolean pilhaCheia(){
        return topo == (elementos.length-1);
    }
    
    public boolean pilhaVazia(){
        return topo == -1;
    }
    
    /**
     * Classe que insere elementos num array de inteiros
     * 
     * @param elemento int que será inserido na pilha.
     * @throws Exception Stack Overflow
     */
    public void push(int elemento) throws Exception{
        if (!pilhaCheia())
            elementos[++topo] = elemento;
        else
            throw new Exception("Stack Overflow");
    }
    
    public int pop() throws Exception{
        
        if(!pilhaVazia())
            return elementos[topo--];
        else
            throw new Exception("Stack Underflow!");
    }
    
    public int top() throws Exception{
        if(!pilhaVazia())
            return elementos[topo];
        else
            throw new Exception("Stack Underflow");
    }
}
