/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EstruturaDeDados.Pilhas;

/**
 *
 * @author aluno
 */
public class Principal {
    public static void main(String[] args) {
        Pilha pil = new Pilha(6);
        
        try {
            int i = 1;
            while (!pil.pilhaCheia()) {
                pil.push(i*10);
                i++;
            }
            
            while(!pil.pilhaVazia()){
                System.out.println("Dado " + pil.pop());
            }
        } catch (Exception e) {
        }
        
    }
}
