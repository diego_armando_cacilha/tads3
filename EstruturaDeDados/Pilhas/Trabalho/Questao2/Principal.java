/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EstruturaDeDados.Pilhas.Trabalho.Questao2;

/**
 *
 * @author Diego
 */
public class Principal {
    public static void main(String[] args){
        int[] meuArray = new int[5];
        meuArray[0] = 20;
        meuArray[1] = 2;
        meuArray[2] = 220;
        meuArray[3] = 22;
        meuArray[4] = 3;
        
        Pilha pil1 = new Pilha(5);
        Pilha pil2 = new Pilha(5);
        try {
            pil1.push(meuArray[0]);
        } catch (Exception e) {
            System.out.println(e);
        }
        
        
        if(!pil1.pilhaCheia()){
            int i = 1;
            while(i <=5){
                try {
                    while(pil1.top() > meuArray[i])
                        pil2.push(pil1.pop());
                } catch (Exception e) {
                    System.out.println(e);
                }
                
                try {
                    pil1.push(meuArray[i]);
                } catch (Exception e) {
                    System.out.println(e);
                }
                    
                try {
                    while(!pil2.pilhaVazia())
                        pil1.push(pil2.pop());
                } catch (Exception e) {
                    System.out.println(e);
                }
                
                
                i++;
            }
            
        }
//        int i = 0;
//        while(!pil1.pilhaCheia() && pil1.top() > 2){
//            
//            if(pil1.pilhaVazia()){
//                pil1.push(meuArray[i]);
//            }else{
//                //if(pil1.top() > meuArray[i]){
//                    while(!pil1.pilhaVazia()){
//                        pil2.push(pil1.pop());
//                    }
//                    pil1.push(meuArray[i]);
//                    
//                    while(!pil2.pilhaVazia()){
//                        pil1.push(pil2.pop());
//                    }
//                //}
//            }
//            i++;
//        }
        
        try {
            while(!pil1.pilhaVazia()){
                System.out.println(pil1.pop());
            }
            
        } catch (Exception e) {
            System.out.println(e);
        }
    
            
    }
}
