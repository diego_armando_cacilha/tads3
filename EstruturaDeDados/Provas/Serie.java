/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TadsIII.EstruturaDeDados.Provas;

import java.util.Scanner;

/**
 *
 * @author aluno
 */
public class Serie {

    public static void main(String[] args) {

        try {
            Scanner scanner = new Scanner(System.in);

            System.out.print("Digite o valor para N: ");

            int n = scanner.nextInt();

            double valor = serie(n);

            System.out.println("Calculado recursivo: " + valor);

            valor = 0;

            for (int i = 1; i <= n; i++) {
                valor += (1 + Math.pow(i, 2)) / i;
            }

            System.out.println("Calculado iterativo: " + valor);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static double serie(int n) {

        if (n > 1) {
            return (serie(n - 1) + (1 + Math.pow(n, 2)) / n);
        } else {
            return (1 + Math.pow(n, 2)) / n;
        }
    }
}
