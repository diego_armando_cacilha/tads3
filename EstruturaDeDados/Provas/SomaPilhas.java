/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TadsIII.EstruturaDeDados.Provas;

import EstruturaDeDados.Pilhas.Pilha;

import java.util.Scanner;

public class SomaPilhas {

    public static void main(String[] args) {

        try {
            Scanner scanner = new Scanner(System.in);

            System.out.print("Digite o 1o numero: ");
            String num1 = scanner.nextLine();

            System.out.print("Digite o 2o numero: ");
            String num2 = scanner.nextLine();

            Pilha n1 = new Pilha(num1.length());
            Pilha n2 = new Pilha(num2.length());
            Pilha resultado = new Pilha(num1.length() + num2.length());

            for (int i = 0; i < num1.length(); i++)
                n1.push(Integer.parseInt(Character.toString(num1.charAt(i))));

            for (int i = 0; i < num2.length(); i++)
                n2.push(Integer.parseInt(Character.toString(num2.charAt(i))));


            boolean up = false;
            int r = 0;

            while ((!n1.pilhaVazia()) || (!n2.pilhaVazia())) {

                String v1 = null, v2 = null;

                if (!n1.pilhaVazia())
                    v1 = Integer.valueOf(n1.pop()).toString();

                if (!n2.pilhaVazia())
                    v2 = String.valueOf(n2.pop());

                if ((v1 != null) && (v2 != null))
                    r = Integer.parseInt(v1) + Integer.parseInt(v2);
                else
                    r = (v1 == null) ? Integer.parseInt(v2) : Integer.parseInt(v1);

                if (up) {
                    r+=1;
                    up = false;
                }

                if (r < 9)
                    resultado.push(r);
                else {
                    resultado.push(r - 10);
                    up = true;
                }
            }

            System.out.print("resultado = ");

            while (!resultado.pilhaVazia())
                System.out.print(resultado.pop());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
