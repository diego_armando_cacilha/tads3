package EstruturaDeDados.Recursividade;

/**
 * 1 Implementar um método recursivo para calcular o produto de dois números inteiros arbitrários usando apenas o operador de soma.
 * 2 Implementar um método para imprimir uma String de maneira invertida.
 * 3 Implementar um método recursivo para calcular o MDC de 2 núemros pelo algoritmo de Euclides.
 * 4 Implementar um método para calcular uma operação de potência de maneira recursiva (fornecer base e expoente).
 * 
 * 
 * Segunda-feira: Programação III;
 * Terça-feira: Estrutura de Dados;
 * Quarta-feira: Sistemas Operacionais;
 * Quinta-feira: Programação III e Estrutura de Dados;
 * Sexta-feira: Banco de Dados.
 */