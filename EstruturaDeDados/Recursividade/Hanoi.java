/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EstruturaDeDados.Recursividade;

/**
 *
 * @author guest-SYeg79
 */
public class Hanoi {
    
    public void hanoi(int n, char de, char para, char tmp){
        if(n == 1){
            System.out.println("Move disco: "+ de + " para a torre "+ para);
        }else{
            hanoi(n-1, de, tmp, para);
            System.out.println("Move disco: "+ de + " para a torre: "+ para);
            hanoi(n-1, tmp, para, de);
        }
    }
    
}
