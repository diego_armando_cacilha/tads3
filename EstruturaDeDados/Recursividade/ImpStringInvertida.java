/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EstruturaDeDados.Recursividade;

/**
 *
 * @author aluno
 */
public class ImpStringInvertida {
    
    public void inverte(String texto){
        
        if(texto.length() == 1)
        {
            System.out.println(texto);
        }else
        {
            System.out.print(texto.charAt(texto.length()-1));
            inverte(texto.substring(0, texto.length()-1));
        }
        
    }
}
