/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EstruturaDeDados.Recursividade;

/**
 *
 * @author aluno
 */
public class SomaRecursiva {
    
    /* produto (2, 3)
    2 + produto(2, 2)
    2 + produto(2, 1) */
    
    public int produto(int num1, int num2)
    {
        if (num2 == 1)
            return num1;
        else {
            return num1 + produto(num1, num2-1);
        }
    }
    
}
