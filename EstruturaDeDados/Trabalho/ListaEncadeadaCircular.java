/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EstruturaDeDados.Trabalho;

/**
 *
 * @author aluno
 */

public class ListaEncadeadaCircular {

    Node inicio;
    int tamanho;

    public ListaEncadeadaCircular () {

        tamanho = 0;
        inicio = null;
    }

    public boolean listaVazia () {
        return tamanho == 0;
    }

    public void inserirInicio (String elemento) {

        Node novo = new Node(elemento);

        if (!listaVazia()) {
            Node tmp = inicio;
            
            while(tmp.proximo != inicio){
                tmp = tmp.proximo;
            }
            novo.proximo = inicio;
            inicio = novo;
            tmp.proximo = inicio;
        }
        else {
            inicio = novo;
            inicio.proximo = inicio;
        }

        tamanho++;
    }

    public void inserirFinal (String elemento) {

        Node novo = new Node(elemento);

        if (!listaVazia()) {
            Node tmp = inicio;

            while (tmp.proximo != inicio) {
                tmp = tmp.proximo;
            }

            tmp.proximo = novo;
            novo.proximo = inicio;
        }
        else {
            inicio = novo;
            inicio.proximo = inicio;
        }

        tamanho++;
    }

    public void inserirPosicao (int posicao, String elemento) {

        if (posicao <= tamanho) {

            if (posicao == 1)
                inserirInicio(elemento);
            else {
                if (posicao == tamanho)
                    inserirFinal(elemento);
                else {
                    Node novo = new Node(elemento);
                    Node tmp = inicio;
                    int indice = 1;

                    while ((tmp.proximo != null) && (indice < posicao-1)) {
                        tmp = tmp.proximo;
                        indice++;
                    }

                    novo.proximo = tmp.proximo;
                    tmp.proximo = novo;
                }
            }

            tamanho++;
        }
    }

    public void imprimeLista () {

        if (!listaVazia()) {
            Node tmp = inicio;

            while (tmp.proximo != inicio) {
                System.out.println("Elemento: " + tmp.nome);
                tmp = tmp.proximo;
            }
            System.out.println("Elemento: " + tmp.nome);
        }
    }

    public void removerInicio () {

        if (!listaVazia()) {
            Node tmp = inicio;  // necessario guarda a referencia apenas se fizer uso do dado!

            if (tamanho > 1) {
                while (tmp.proximo != inicio) {
                    tmp = tmp.proximo;
                }

                inicio = inicio.proximo;
                tmp.proximo = inicio;
            }
            else {
                inicio = null;
            }

            tamanho--;
        }
    }

    public void removerFinal () {

        if (!listaVazia()) {
            Node ant = null, tmp = inicio;

            while (tmp.proximo != inicio) {
                ant = tmp;
                tmp = tmp.proximo;
            }

            if (ant != null)
                ant.proximo = inicio;
            else
                inicio = null;

            tamanho--;
        }
    }

    public void removerPosicao (int posicao) {

        if (!listaVazia()) {
            if (posicao <= tamanho) {

                if (posicao == 1)
                    removerInicio();
                else {
                    if (posicao == tamanho)
                        removerFinal();
                    else {
                        Node ant = null, tmp = inicio;
                        int indice = 1;

                        while ((tmp.proximo != inicio) && (indice < posicao)) {
                            ant = tmp;
                            tmp = tmp.proximo;
                            indice++;
                        }

                        ant.proximo = tmp.proximo;
                    }
                }
            }

            tamanho--;
        }
    }
    
    public void josephus(String nome, int sorteio){
        if(!listaVazia()){
            Node ant = null;
            Node tmp = inicio;
            
            while(!tmp.getNome().equals(nome))
                tmp = tmp.proximo;
            
            while(tmp.proximo != tmp){
                for(int i = 0; i < sorteio; i++){
                    ant = tmp;
                    tmp = tmp.proximo;
                }
                
                System.out.println("Soldado que sai na rodada: " + tmp.getNome());
                
                ant.proximo = tmp.proximo;
                tmp = tmp.proximo;
            }
            
            System.out.println("Soltado sorteado: " + tmp.getNome());
        }
    }
}