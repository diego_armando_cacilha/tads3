/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EstruturaDeDados.Trabalho;

/**
 *
 * @author aluno
 */
public class Node {
    
    String nome;
    Node proximo;

    public Node (String nome) {
        this.nome = nome;
        this.proximo = null;
    }

    public String getNome() {
        return nome;
    }
    
}
