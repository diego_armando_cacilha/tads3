
package TadsIII.EstruturaDeDados.Trabalho.TentativaTrabalho;

public class ListaEncadeadaCircular {

    Noh inicio;
    int tamanho;

    public ListaEncadeadaCircular () {

        tamanho = 0;
        inicio = null;
    }


    public boolean listaVazia () {

        return tamanho == 0;
    }

    public void inserirNome (String nome) {

        Noh tmp = new Noh(nome);

        if (!listaVazia()) {
            tmp.proximo = inicio;
            inicio = tmp;
        }
        else {
            //inicio.proximo = inicio;
            inicio = tmp;
        }

        tamanho++;
    }



    public void imprimeLista () {

        if (!listaVazia()) {
            Noh tmp = inicio;

            while (tmp != null) {
                System.out.println("Elemento: " + tmp.nome);
                tmp = tmp.proximo;
            }
        }
    }
    
    
    public void removerFinalLista () {

        if (!listaVazia()) {
            Noh tmp = inicio;  // necessario guarda a referencia apenas se fizer uso do dado!

            if (tamanho > 1) {
                //while (tmp.proximo == inicio) {
                //    tmp = tmp.proximo;
                //}

                inicio = inicio.proximo;
                tmp.proximo = inicio;
            }
            else {
                inicio = null;
            }

            tamanho--;
        }
    }

/*
    public void inserirFinal (int elemento) {

        Noh novo = new Noh(elemento);

        if (!listaVazia()) {
            Noh tmp = inicio;

            while (tmp.proximo != inicio) {
                tmp = tmp.proximo;
            }

            tmp.proximo = novo;
            novo.proximo = inicio;
        }
        else {
            inicio = novo;
            inicio.proximo = inicio;
        }

        tamanho++;
    }


    public void inserirPosicao (int posicao, int elemento) {

        if (posicao <= tamanho) {

            if (posicao == 1)
                inserirNome(elemento);
            else {
                if (posicao == tamanho)
                    inserirFinal(elemento);
                else {
                    Noh novo = new Noh(elemento);
                    Noh tmp = inicio;
                    int indice = 1;

                    while ((tmp.proximo != null) && (indice < posicao-1)) {
                        tmp = tmp.proximo;
                        indice++;
                    }

                    novo.proximo = tmp.proximo;
                    tmp.proximo = novo;
                }
            }

            tamanho++;
        }
    }

    public void removerFinalLista () {

        if (!listaVazia()) {
            Noh tmp = inicio;  // necessario guarda a referencia apenas se fizer uso do dado!

            if (tamanho > 1) {
                while (tmp.proximo != inicio) {
                    tmp = tmp.proximo;
                }

                inicio = inicio.proximo;
                tmp.proximo = inicio;
            }
            else {
                inicio = null;
            }

            tamanho--;
        }
    }


    public void removerFinal () {

        if (!listaVazia()) {
            Noh ant = null, tmp = inicio;

            while (tmp.proximo != inicio) {
                ant = tmp;
                tmp = tmp.proximo;
            }

            if (ant != null)
                ant.proximo = inicio;
            else
                inicio = null;

            tamanho--;
        }
    }


    public void removerPosicao (int posicao) {

        if (!listaVazia()) {
            if (posicao <= tamanho) {

                if (posicao == 1)
                    removerFinalLista();
                else {
                    if (posicao == tamanho)
                        removerFinal();
                    else {
                        Noh ant = null, tmp = inicio;
                        int indice = 1;

                        while ((tmp.proximo != inicio) && (indice < posicao)) {
                            ant = tmp;
                            tmp = tmp.proximo;
                            indice++;
                        }

                        ant.proximo = tmp.proximo;
                    }
                }
            }

            tamanho--;
        }
    }*/
}
