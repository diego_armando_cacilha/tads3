package TadsIII.EstruturaDeDados.Trabalho.TentativaTrabalho;
/**
 *
 * @author Diego
 */
public class Principal {
    public static void main(String[] args) {
        ListaEncadeadaCircular lista = new ListaEncadeadaCircular();
        
        
        lista.inserirNome("Lais");
        lista.inserirNome("Diego");
        lista.inserirNome("Sophie");
        
        lista.removerFinalLista();
        lista.removerFinalLista();
        lista.removerFinalLista();
        
        lista.imprimeLista();
    }
    
}
