//mattheus.krueger@blumenau.ifc.edu.br
package TadsIII.ProgramacaoIII.AbrigoDeCaes;

public class Abrigo 
{
    Cachorro[] caes = new Cachorro[10];
    
    public void armazenaCaes(Cachorro cachorro)
    {
        for(int i = 0; i<10;i++){
            if(caes[i] == null){
                caes[i] = cachorro; 
                i = 10;
            }
        }
    }
    
    public void removeCaes(int index)
    {
        caes[index] = null;
    }
    public Cachorro buscaCaes(int index)
    {
        return caes[index];
    }
    public void infoTodosCaes()
    {
        int i = 0;
        try 
        {
            for(i = 0; i<caes.length;i++)
            {
                caes[i].Imprimir();
            }
        } catch (Exception e) 
        {
            if(i == 0)
            {
                System.out.println("Não há cães no abrigo");
            }else 
            {
                System.out.println("Há apenas "+i+" cachorro(s) no abrigo");
            }
        }

    }
    
    public void infoCaoNome()
    {
        try {
            for (int i =0; i< caes.length;i++)
            {
                System.out.println(caes[i].getNome());
            }
        } catch (Exception e) {
        }

    }
    public void infoCaoIdade()
    {
        try {
            for (int i =0; i< caes.length;i++)
            {
                System.out.println(caes[i].getIdade());
            }
        } catch (Exception e) {
        }

    }
    public void infoCaoRaca()
    {
        try {
            for (int i =0; i< caes.length;i++)
            {
                System.out.println(caes[i].getRaca());
            }
        } catch (Exception e) {
        }
    }
    
}
