package TadsIII.ProgramacaoIII.AbrigoDeCaes;

public class Cachorro 
{
    private String nome;
    private int idade;
    private String raca;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public String getRaca() {
        return raca;
    }

    public void setRaca(String raca) {
        this.raca = raca;
    }
    
    public void Imprimir()
    {
        System.out.println("Nome: "+nome);
        System.out.println("Idade: "+idade);
        System.out.println("Raça: "+raca);
    }
    
    public Cachorro(){
        
    }
    
    public Cachorro(String nome, int idade, String raca)
    {
        this.idade = idade;
        this.nome = nome;
        this.raca = raca;
    }
}
