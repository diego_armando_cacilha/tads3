package TadsIII.ProgramacaoIII.AbrigoDeCaes;

/**
 *
 * @author aluno
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Cachorro ca1 = new Cachorro("Totó", 2, "Pastor Suíço");
        Cachorro ca2 = new Cachorro("Maia", 1, "Pastor Belga");
        
        Abrigo abri = new Abrigo();
        abri.armazenaCaes(ca1);
        abri.armazenaCaes(ca2);
        
        abri.infoCaoNome();
        abri.infoTodosCaes();
        
    }
    
}
