/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProgramacaoIII.Elevadores;

/**
 *
 * @author Diego
 */
public class Elevador {
    
    private int andarAtual = 0;
    private int totalAndares;
    private int capacidade;
    private int totalPessoas;
    
    public Elevador(int capacidade, int totalAndares){
        this.capacidade = capacidade;
        this.totalAndares = totalAndares;
    }
    
    public void entrada(int totalPessoas)
    {
        if (this.capacidade <= this.totalPessoas)
        {
            this.totalPessoas += totalPessoas;
        }else{
            System.out.println("Limite de passageiros atingido.");
        }
    }
    
    public void sai(int i)
    {
        /*
        eu poderia ter colocado um while para verificar se o numero de passageiros que estão saindo é menor do que a quantidade atual de passageiros,
        mas como estou trabalhando com valores fixos, o while entraria num looping infinito. Por isso estou usando o if.
        */
        if (this.capacidade < i)
        {
            this.totalPessoas = 0;
        }else
        {
            this.capacidade -= i;
        }
        
    }
    
    public void sobeUmAndar()
    {
        if(this.totalAndares == this.andarAtual)
        {
            System.out.println("Você já está no ultimo andar.");
        }else
        {
            this.andarAtual++;
        }
    }
    
    public void desceUmAndar()
    {
        if(this.andarAtual == 0)
        {
            System.out.println("Você já está no térreo.");
        }else
        {
            this.andarAtual--;
        }
    }

    public void setAndarAtual(int andareAtual) {
        this.andarAtual = andareAtual;
    }

    public void setCapacidade(int capacidade) {
        this.capacidade = capacidade;
    }

    public void setTotalAndares(int totalAndares) {
        this.totalAndares = totalAndares;
    }

    public void setTotalPessoas(int totalPessoas) {
        this.totalPessoas = totalPessoas;
    }

    public int getAndarAtual() {
        return andarAtual;
    }

    public int getCapacidade() {
        return capacidade;
    }

    public int getTotalAndares() {
        return totalAndares;
    }

    public int getTotalPessoas() {
        return totalPessoas;
    }
    
}
