/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProgramacaoIII.Elevadores;

import TadsIII.ProgramacaoIII.AbrigoDeCaes.Abrigo;
import TadsIII.ProgramacaoIII.AbrigoDeCaes.Cachorro;

/**
 *
 * @author Diego
 */
public class Main {
    
    public static void main(String[] args) {
        Abrigo a = new Abrigo();
        Cachorro ca = new Cachorro();
        a.armazenaCaes(ca);
        
        Elevador el = new Elevador(10, 20);
        
        el.sobeUmAndar();
        el.sobeUmAndar();
        el.sobeUmAndar();
        el.sobeUmAndar();
        el.sobeUmAndar();
        
        el.desceUmAndar();
        System.out.println(el.getAndarAtual());
    }
    
}
