/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProgramacaoIII.Excecoes.Exercicio1;

/**
 *
 * @author aluno-informatica
 */
public class Disciplina {
    private int ano;
    private String nome;
    
    public Disciplina(String ano, String nome)
    {
        try {
            this.ano = Integer.parseInt(ano);
        } catch (Exception e) {
            System.out.println("O ano informado é inválido");
        }
        this.nome = nome;
    }
}
