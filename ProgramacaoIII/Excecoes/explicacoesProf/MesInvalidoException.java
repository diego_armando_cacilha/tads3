/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProgramacaoIII.Excecoes.explicacoesProf;

/**
 *
 * @author aluno-informatica
 */
public class MesInvalidoException extends Exception{
    public MesInvalidoException(String texto){
        super(texto);
    }
}
