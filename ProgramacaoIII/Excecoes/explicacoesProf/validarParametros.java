/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProgramacaoIII.Excecoes.explicacoesProf;

/**
 *
 * @author aluno-informatica
 */
public class validarParametros {
    private int dia;
    private int mes;
    private int ano;
    
    private void validarParametros(int dia, int mes, int ano) throws ArgumentoInvalidoException, MesInvalidoException
    {
        if(mes < 1 || mes > 12)
        {
            throw new MesInvalidoException("Mês inválido");
        }
        
        if(dia < 1 || dia > 30)
        {
            throw new MesInvalidoException("Dia inválido");
        }
        
        if(ano < 2000 || ano > 2030)
        {
            throw new MesInvalidoException("Ano inválido");
        }
        
        this.dia = dia;
        this.mes = mes;
        this.ano = ano;
    }
}
