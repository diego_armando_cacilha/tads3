package ProgramacaoIII.Exercicios.Lista02;

import ProgramacaoIII.Exercicios.Lista02.excecoes.LimiteInvalido;
import java.util.concurrent.ExecutionException;

/**
 *
 * @author Diego Armando Cacilha
 */
public class ContaCorrente {
    private double saldoLimite; //essa variável corresponde a variável 'limite' solicitada no exercicio
    private double saldo;
    private double limiteContratado; //essa variável corresponde a variável 'valorLimite' solicitada no exercicio

    public ContaCorrente(double saldo, double valorMaxLimite) throws Exception{
        
        if(valorMaxLimite < 0){
            throw new LimiteInvalido("Meu erro");
            //throw new Exception("O valor do limite não pode ser menor que 0 (zero).");
        }else
        {
            this.saldoLimite = valorMaxLimite;
            this.saldo = saldo;
            this.limiteContratado = valorMaxLimite;
        }
        
    }
    public void depositar(double valorDeposito) throws Exception{
        if(valorDeposito < 0)
        {
            throw new Exception("Informe um valor maior que zero");
        }else
        {
            this.saldo += valorDeposito;
        }
    }
    
    public void sacar(double valorSaque) throws Exception{
        if(valorSaque > (this.saldo + this.saldoLimite))
        {
            throw new Exception("Saldo insuficiente");
        }else 
        {
            if(this.saldo >= valorSaque)
            {
                this.saldo -= valorSaque;
            }else
            {
                valorSaque -= this.saldo;
                this.saldo = 0;
                this.saldoLimite -= valorSaque;
            }
            
        }
        
    }

    public void setLimiteContratado(double novoLimite) throws Exception {
        //se o novo limite contratado for maior que o limite anterior, não há necessidade de verificar se o cliente está atualmente usando o limite.
        if(novoLimite > this.limiteContratado)
        {
            double diferenca = novoLimite - this.limiteContratado;
            this.limiteContratado = novoLimite;
            this.saldoLimite += diferenca;
        }else
        {
            if(this.saldoLimite == novoLimite)
            {
                throw new Exception("O novo limite informado é igual ao atual");
            }else
            {
                this.saldoLimite = ((this.limiteContratado - this.saldoLimite) + novoLimite);
            }
            
        }
        
        
    }

    
    public double getLimiteContratado() {
        return limiteContratado;
    }

    public double getSaldo() {
        return saldo;
    }

    public double getSaldoLimite() {
        return saldoLimite;
    }
    
    
}
