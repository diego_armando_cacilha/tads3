package ProgramacaoIII.Exercicios.Lista02;

import java.util.logging.Level;
import java.util.logging.Logger;
import oracle.jrockit.jfr.tools.ConCatRepository;

/**
 *
 * @author Diego Armando Cacilha
 */
public class Principal {
    public static void main(String[] args){
        ContaCorrente conta = null;
        try {
            conta = new ContaCorrente(200, 100);
        } catch (Exception ex) {
            System.out.println(ex);
        }
         
        try {
            conta.sacar(300);
        } catch (Exception ex) {
            System.out.println(ex);
        }
        
        
        System.out.println("Limite contratado: R$" + conta.getLimiteContratado());
        System.out.println("Saldo atual: R$" + conta.getSaldo());
        System.out.println("Saldo do limite atual: R$" + conta.getSaldoLimite());
        try {
            conta.setLimiteContratado(100);
        } catch (Exception ex) {
            System.out.println(ex);
        }
        System.out.println("Limite contratado: R$" + conta.getLimiteContratado());
        System.out.println("Saldo atual: R$" + conta.getSaldo());
        System.out.println("Saldo do limite atual: R$" + conta.getSaldoLimite());
        
        try {
            conta.setLimiteContratado(400);
        } catch (Exception e) {
        }
        
        System.out.println("Limite contratado: R$" + conta.getLimiteContratado());
        System.out.println("Saldo atual: R$" + conta.getSaldo());
        System.out.println("Saldo do limite atual: R$" + conta.getSaldoLimite());
    }
}
