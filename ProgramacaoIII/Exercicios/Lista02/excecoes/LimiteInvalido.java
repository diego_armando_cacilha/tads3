/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProgramacaoIII.Exercicios.Lista02.excecoes;

/**
 *
 * @author Diego Armando Cacilha
 */
public class LimiteInvalido extends Exception{
    
    public LimiteInvalido(String msg){
        super(msg);
    }
}
