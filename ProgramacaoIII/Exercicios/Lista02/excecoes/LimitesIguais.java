package ProgramacaoIII.Exercicios.Lista02.excecoes;

/**
 *
 * @author aluno
 */
public class LimitesIguais extends Exception{
    
    public LimitesIguais(String msg)    
    {
        super(msg);
    }
}
