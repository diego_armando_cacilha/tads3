/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProgramacaoIII.Exercicios.Lista03._01;

/**
 *
 * @author aluno
 */
public class Vip extends Ingresso {
    double valorAdicional;
    
    public Vip(double valor, String data, double valorAdicional){
        this.valor = valor;
        this.data = data;
        this.valorAdicional = valorAdicional;
    }
    public Vip(String data, double valorAdicional){
        this.data = data;
        this.valorAdicional = valorAdicional;
    }

    public double getValorTotal() {
        return (valorAdicional+valor);
    }
    
}
