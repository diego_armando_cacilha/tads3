/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProgramacaoIII.Exercicios.Lista03._02;

/**
 *
 * @author aluno
 */
public class Principal {
    public static void main(String[] args) {
        Novo no = new Novo();
        
        no.preco = 50000.00;
        no.precoAdicional = 20;
        
        System.out.println(no.getPrecoTotal());
        
        Usado us = new Usado();
        
        us.preco = 100000.00;
        us.desconto = 20;
        us.endereco = "Rua Pindamonhagaba";
        System.out.println(us.getEndereco());
        System.out.println(us.precoTotal());
        
    
    }
}
