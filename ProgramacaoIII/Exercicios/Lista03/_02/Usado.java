/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProgramacaoIII.Exercicios.Lista03._02;

/**
 *
 * @author aluno
 */
public class Usado extends Imovel {
    double desconto;
    
    public double precoTotal(){
        return (preco - ((preco * desconto) / 100));
    }
    
    public String getEndereco() {
        return endereco;
    }
    public void setDesconto(double desconto) {
        this.desconto = desconto;
    }
}
