/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProgramacaoIII.Exercicios.Lista03._04;

/**
 *
 * @author aluno
 */
public class AMD extends Processador{
    private double frequenciaAMD;
    private int cache;
    
    public AMD(String nome, double soquete, double frequencia, int qtdNucleos, double frequenciaAMD, int cache) {
        super(nome, soquete, frequencia, qtdNucleos);
        this.frequenciaAMD = frequenciaAMD;
        this.cache = cache;
    }
    
    public double getFrequenciaAMD() {
        return frequenciaAMD;
    }

    public int getCache() {
        return cache;
    }

    public void setFrequenciaAMD(double frequenciaAMD) {
        this.frequenciaAMD = frequenciaAMD;
    }

    public void setCache(int cache) {
        this.cache = cache;
    }
    
}
