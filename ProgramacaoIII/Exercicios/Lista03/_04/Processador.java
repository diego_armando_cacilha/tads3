package ProgramacaoIII.Exercicios.Lista03._04;

/**
 *
 * @author Diego Armando Cacilha
 */
public class Processador {
    private String nome;
    private double soquete;
    private double frequencia;
    private int qtdNucleos;

    public boolean multicore(){
        if(qtdNucleos > 1){
            return true;
        }else return false;
    }
    public Processador(String nome, double soquete, double frequencia, int qtdNucleos){
        this.nome = nome;
        this.soquete = soquete;
        this.frequencia = frequencia;
        this.qtdNucleos = qtdNucleos;
    }
    public String getNome() {
        return nome;
    }

    public double getSoquete() {
        return soquete;
    }

    public double getFrequencia() {
        return frequencia;
    }

    public int getQtdNucleos() {
        return qtdNucleos;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setSoquete(double soquete) {
        this.soquete = soquete;
    }

    public void setFrequencia(double frequencia) {
        this.frequencia = frequencia;
    }

    public void setQtdNucleos(int qtdNucleos) {
        this.qtdNucleos = qtdNucleos;
    }
    
    
    
}
