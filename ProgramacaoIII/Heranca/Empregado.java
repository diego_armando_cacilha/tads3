package ProgramacaoIII.Heranca;

/**
 *
 * @author aluno
 */
public class Empregado {
    private String nome;

    public Empregado(String nome)
    {
        this.nome = nome;
    }
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    @Override
    public String toString()
    {
        return nome;
    }
    
    
}
