/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProgramacaoIII.Heranca.Exercicio.Exercicio1;

/**
 *
 * @author aluno
 */
public class Imovel {
    private String endereco;
    private double area;
    private double preco;

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public double getPreco() {
        return preco;
    }
    
     public void calculoPreco(){
         this.preco = 0;
     }
    
    public Imovel(String endereco, double area){
        this.endereco = endereco;
        this.area = area;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }
}
