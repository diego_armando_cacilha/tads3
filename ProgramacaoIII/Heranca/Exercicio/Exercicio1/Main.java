package ProgramacaoIII.Heranca.Exercicio.Exercicio1;

public class Main {
    public static void main(String[] args) {
        Novo imovelNovo1 = new Novo(100, "Rua Blá");
        Velho imovelNovo2 = new Velho(200, "Rua blé");
        Novo imovelNovo3 = new Novo(300, "Rua Blí");
        Novo imovelNovo4 = new Novo(400, "Rua Bló");
        
        Imovel[] imo = new Imovel[4];
        
        imo[0] = imovelNovo1;
        imo[1] = imovelNovo2;
        imo[2] = imovelNovo3;
        imo[3] = imovelNovo4;
        
        
        for(int i = 0; i < imo.length; i++)
            System.out.println(imo[i].getPreco());
//        
//        System.out.println(imovelNovo1.getCalPreco());
//        System.out.println(imovelNovo1.getCub());
        
    }
}
