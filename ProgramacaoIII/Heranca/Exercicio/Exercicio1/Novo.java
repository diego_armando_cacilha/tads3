/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProgramacaoIII.Heranca.Exercicio.Exercicio1;

/**
 *
 * @author aluno
 */
public class Novo extends Imovel{
    private double cub = 2000;
    
    public Novo(double area, String endereco){
        super(endereco, area);
        calculoPreco();
    }
    
    @Override
    public void calculoPreco(){
        this.setPreco(2 * cub * this.getArea());
    }
    
    public double getCub() {
        return cub;
    }

    public void setCub(double cub) {
        this.cub = cub;
    }
}
