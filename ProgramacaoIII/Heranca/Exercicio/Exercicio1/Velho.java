/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProgramacaoIII.Heranca.Exercicio.Exercicio1;

/**
 *
 * @author aluno
 */
public class Velho extends Imovel{
    private double cub;
    
    
    public Velho(double area, String endereco){
        super(endereco, area);
    }
    
    @Override
    public void calculoPreco(){
        this.setPreco(2 * cub * this.getArea());
    }

    public void setCub(double cub) {
        this.cub = cub;
    }

    public double getCub() {
        return cub;
    }
}
