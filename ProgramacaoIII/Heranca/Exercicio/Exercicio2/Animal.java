/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProgramacaoIII.Heranca.Exercicio.Exercicio2;

/**
 *
 * @author aluno
 */
public class Animal {
    private String nome;
    private int idade;
    private String som;

    public Animal(String nome, int idade, String som){
        this.nome = nome;
        this.idade = idade;
        this.som = som;
    }
    
    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSom() {
        return som;
    }

    public void setSom(String som) {
        this.som = som;
    }
    
    
}
