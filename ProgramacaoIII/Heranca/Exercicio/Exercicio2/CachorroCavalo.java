/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProgramacaoIII.Heranca.Exercicio.Exercicio2;

/**
 *
 * @author aluno
 */
public class CachorroCavalo extends Animal{
    private boolean correr;
    
    public CachorroCavalo(String nome, int idade, String som, boolean corre) {
        super(nome, idade, som);
        this.correr = correr;
    }

    public boolean isCorrer() {
        return correr;
    }

    public void setCorrer(boolean correr) {
        this.correr = correr;
    }
    
}
