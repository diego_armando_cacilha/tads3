package ProgramacaoIII.Heranca.Exercicio.Exercicio2;
public class Main {
    public static void main(String[] args){
        CachorroCavalo cachorro = new CachorroCavalo("Totó", 4, "Au Au!", true);
        CachorroCavalo cavalo = new CachorroCavalo("Trovão", 6, "RRSssssss!", false);
        Preguica preguica = new Preguica("Felisbina", 2, "zzZZZzzzzZZZZzzz...", true);
        
        Zoologico zoo = new Zoologico(3);
        zoo.enjaular(cavalo);
        zoo.enjaular(cachorro);
        zoo.enjaular(preguica);
        //zoo.enjaular(cavalo);
        
        for(int i = 0;i < zoo.jaula.length; i++){
            System.out.println(zoo.jaula[i].getSom());
        }
    }
}
