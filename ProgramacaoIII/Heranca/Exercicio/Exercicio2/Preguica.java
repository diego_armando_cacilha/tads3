package ProgramacaoIII.Heranca.Exercicio.Exercicio2;

public class Preguica extends Animal{
    private boolean subirArvore;
    
    public Preguica(String nome, int idade, String som, boolean subirArvore) {
        super(nome, idade, som);
    }

    public boolean isSubirArvore() {
        return subirArvore;
    }

    public void setSubirArvore(boolean subirArvore) {
        this.subirArvore = subirArvore;
    }
    
}
