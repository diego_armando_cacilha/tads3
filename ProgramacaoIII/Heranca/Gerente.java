/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProgramacaoIII.Heranca;

/**
 *
 * @author aluno
 */
public class Gerente extends Empregado{
    private double bonus;
    
    public Gerente(String nome, double bonus)
    {
        super(nome);
        this.bonus = bonus;
    }
    
    @Override
    public String toString()
    {
        return super.toString() + " - " + bonus;
    }
    
}
