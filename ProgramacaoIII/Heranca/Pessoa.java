/*
- No UML o # representa atributos ou métodos protected
- 
*/
package ProgramacaoIII.Heranca;

/**
 *
 * @author Diego Armando Cacilha
 */
public class Pessoa {
    private String nome;
    private int idade;
    
    protected void setNome(String nome)
    {
        this.nome = nome;
    }
    public String getNome()
    {
        return this.nome;
    }
    public void setIdade(int idade)
    {
        this.idade = idade;
    }
    public int getIdade(int idade)
    {
        return this.idade;
    }
}
