/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProgramacaoIII.IO;

import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Diego Armando Cacilha
 */
public class EscreverArquivoTxt {
    public static void main(String[] args) throws IOException {
        System.out.println("Digite um texto: ");
        FileWriter out = null;
        
        try {
            out = new FileWriter("file.txt", true);//o true serve para concatenar o texto digitado com o testo existente no arquivo
        } catch (IOException ex) {
            Logger.getLogger(EscreverArquivoTxt.class.getName()).log(Level.SEVERE, null, ex);
        }
        byte a = (byte)System.in.read();
        while (a!='\n') {
            out.write(a);
            a=(byte)System.in.read();
        }
        out.close();
        
    }
    
}
