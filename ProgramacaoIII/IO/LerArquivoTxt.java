/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProgramacaoIII.IO;

import java.io.*;

/**
 *
 * @author guest-5ESftm
 */
public class LerArquivoTxt {
    public static void main(String[] args) {
        try {
            FileReader r = new FileReader("test.txt");
            BufferedReader in = new BufferedReader(r);
            String linha = in.readLine();
            while (linha != null) {                
                System.out.println(linha);
                linha = in.readLine();
            }
            in.close();
        } catch (FileNotFoundException e) {
            System.out.println("test.txt não existe");
        } catch (IOException e) {
            System.out.println("Erro de leitura");
        }
        
    }
  
    
}
