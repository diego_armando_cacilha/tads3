/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProgramacaoIII.IO;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author aluno
 */
public class ManuseioArquivos {
    
    public void copia(String origem, String destino){
        try {
            FileReader r = new FileReader(origem);
            BufferedReader in = new BufferedReader(r);
            String linha = in.readLine();
            while (linha != null) {                
                System.out.println(linha);
                linha = in.readLine();//recupera a próxima linha
            }
            in.close();
        } catch (FileNotFoundException e) {
            System.out.println("test.txt não existe");
        } catch (IOException e) {
            System.out.println("Erro de leitura");
        }
    }
}
