/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TadsIII.ProgramacaoIII.InterfaceGrafica.Exercicio;

import java.awt.Container;
import javax.swing.JFrame;

/**
 *
 * @author aluno
 */
public class MeuFrame extends JFrame{
    
    public MeuFrame(){
        setTitle("Janela");
        setSize(500, 500);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        Container painelConteudo = getContentPane();
        painelConteudo.add(new MeuPainel());
    }
}
