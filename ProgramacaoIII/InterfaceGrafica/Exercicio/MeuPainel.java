/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TadsIII.ProgramacaoIII.InterfaceGrafica.Exercicio;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author aluno
 */
public class MeuPainel extends JPanel implements ActionListener{
    
    private JButton bt1                 = new JButton("Calcular");
    private JButton bt2                 = new JButton("Limpar");
    private JLabel valDolar             = new JLabel("Valor em Dolar:");
    private JLabel taxaConversao        = new JLabel("Taxa de Conversão:");
    private JLabel valReais             = new JLabel("Valor em Reais:");
    private JTextField valDolarIO       = new JTextField(40);
    private JTextField taxaConversaoIO  = new JTextField(40);
    private JLabel resultValReais       = new JLabel();
    
    
    public MeuPainel(){
        this.add(valDolar);
        this.add(valDolarIO);
        this.add(taxaConversao);
        this.add(taxaConversaoIO);
        this.add(valReais);
        
        this.resultValReais.setText("Oiii");
        
        this.add(resultValReais);
        
        
        
        this.add(bt1);
        this.add(bt2);
        
        bt1.addActionListener(this);
        bt2.addActionListener(this);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        Object fonteDeEvento = e.getSource();
        
        if(fonteDeEvento == bt1){
            System.out.println("Calcular");
            this.resultValReais.setText(Double.valueOf(this.valDolar.getText()) * Double.valueOf(this.taxaConversaoIO.getText())+"");
            
            
        }else if(fonteDeEvento == bt2){
            System.out.println("Limpar");
        }
        
    }
    
}
