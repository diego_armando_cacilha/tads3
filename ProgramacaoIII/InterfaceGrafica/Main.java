/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TadsIII.ProgramacaoIII.InterfaceGrafica;

import java.awt.Container;
import javax.swing.*;

/**
 *
 * @author aluno
 */
public class Main extends JFrame{
    
    public Main(){
        setTitle("JFrame Exemplo");
        setSize(500, 300);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);//quando fechar a janela, tbm encerra a aplicação
        Container painelConteudo = getContentPane();
        painelConteudo.add(new MeuPainel());
    }
    
    public static void main(String[] args) {
        JFrame meuFrame = new Main();
        meuFrame.setVisible(true);
        
    }
    
}
