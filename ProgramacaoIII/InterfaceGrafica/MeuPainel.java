package TadsIII.ProgramacaoIII.InterfaceGrafica;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JPanel;


/**
 * 
 * @author Diego Armando Cacilha
 */
public class MeuPainel extends JPanel implements ActionListener{
    
    /*
    Cria os botões
    */
    private JButton btVermelho = new JButton("Vermelho");
    private JButton btVerde = new JButton("Verde");
    private JButton btAzul = new JButton("Azul");
    private JButton btAmarelo = new JButton("Amarelo");
    private JButton bt2 = new JButton("Bt2");
    private JButton bt3 = new JButton("Bt3");
    private JButton bt4 = new JButton("Bt4");

    public MeuPainel() {
        /*
        Adiciona os botões ao Frame
        */
        this.add(btVermelho);
        this.add(btVerde);
        this.add(btAzul);
        this.add(btAmarelo);
        this.add(bt2);
        this.add(bt3);
        this.add(bt4);
        
        /*
        Adiciona a escuta para os eventos de botão
        */
        btVermelho.addActionListener(this);
        btVerde.addActionListener(this);
        btAzul.addActionListener(this);
        btAmarelo.addActionListener(this);
    }

    /*
    Verifica o texto do botão e executa a ação correspondente
    */
    @Override
    public void actionPerformed(ActionEvent e) {
        Object fonteDoEvento = e.getSource();
        if(fonteDoEvento == btVermelho){
            System.out.println("Vermelho");
            setBackground(Color.red);
        }else if(fonteDoEvento == btVerde){
            System.out.println("Verde");
            setBackground(Color.GREEN);
        }else if(fonteDoEvento == btAzul){
            System.out.println("Azul");
            setBackground(Color.BLUE);
        }else if(fonteDoEvento == btAmarelo){
            System.out.println("Amarelo");
            setBackground(Color.YELLOW);
        }
    }
}