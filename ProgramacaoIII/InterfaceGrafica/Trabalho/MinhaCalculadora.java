/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TadsIII.ProgramacaoIII.InterfaceGrafica.Trabalho;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author tads
 */
public class MinhaCalculadora extends JPanel implements ActionListener{
    
    private JLabel textArea             = new JLabel();
    private JButton btSoma              = new JButton("+");
    private JButton btSubtrai           = new JButton("-");
    private JButton btDivide            = new JButton("/");
    private JButton btMultiplica        = new JButton("*");
    private JButton btPonto             = new JButton(".");
    private JButton btResultado         = new JButton("=");
    private JButton bt1                 = new JButton("1");
    private JButton bt2                 = new JButton("2");
    private JButton bt3                 = new JButton("3");
    private JButton bt4                 = new JButton("4");
    private JButton bt5                 = new JButton("5");
    private JButton bt6                 = new JButton("6");
    private JButton bt7                 = new JButton("7");
    private JButton bt8                 = new JButton("8");
    private JButton bt9                 = new JButton("9");
    private JButton bt0                 = new JButton("0");

    public MinhaCalculadora() {
        textArea.setSize(100, 40);
        this.add(textArea);
        this.add(btSoma);
        this.add(btSubtrai);
        this.add(btDivide);
        this.add(btMultiplica);
        this.add(btPonto);
        this.add(btResultado);
        this.add(bt1);
        this.add(bt2);
        this.add(bt3);
        this.add(bt4);
        this.add(bt5);
        this.add(bt6);
        this.add(bt7);
        this.add(bt8);
        this.add(bt9);
        this.add(bt0);
    }
    
    

    @Override
    public void actionPerformed(ActionEvent ae) {
        
    }

    
    
}
