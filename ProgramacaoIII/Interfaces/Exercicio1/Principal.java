/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProgramacaoIII.Interfaces.Exercicio1;

/**
 *
 * @author aluno
 */
public class Principal {
    public static void main(String[] args) {
        Quadrado qua = new Quadrado(3);
        
        System.out.println("Área do quadrado é de " + qua.calculaArea() + "m");
        
        Circulo cir = new Circulo(4);
        System.out.printf("%.4f \n", cir.calculaArea());
        
        Retangulo ret = new Retangulo(4, 6);
        System.out.println(ret.calculaArea());
    }
    
    
}
