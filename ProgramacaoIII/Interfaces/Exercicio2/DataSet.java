/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProgramacaoIII.Interfaces.Exercicio2;

public class DataSet {
    private double total;
    private Mensuravel maximo;
    private int count;
    private Mensuravel minimo = null;

    public Mensuravel getMinimo() {
        return minimo;
    }

    public DataSet() {
        total =  0.0;
        count = 0;
    }
    
    /*
     * Adiciona um valor de dados ao conjunto.
     * @param x valor de dados
     */
    public void adiciona (Mensuravel x){
        if(count == 0 || x.getMedida() < this.minimo.getMedida()) //verifica o menor valor
            this.minimo = x;
        
        total += x.getMedida();
        if (count == 0 || maximo.getMedida() < x.getMedida())
            maximo = x;
        count++;
    }
    
    /*
     * Calcula e retorna a média dos valores fornecidos.
     * @return média ou 0.
     */
    public double getMedia() {
        if (count == 0) 
            return 0;
        else
            return (total/count);
    }
    
    /*
     * Retorna o maior valor fornecido.
     * @return maximo.
     */
    public Mensuravel getMaximo() {
        return maximo;
    }
    
}
