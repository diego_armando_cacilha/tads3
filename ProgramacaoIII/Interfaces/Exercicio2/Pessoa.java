package ProgramacaoIII.Interfaces.Exercicio2;

public class Pessoa implements Mensuravel{
    private double altura;
    private String nome;

    public Pessoa(double altura, String nome) {
        this.altura = altura;
        this.nome = nome;
    }

    public double getAltura() {
        return altura;
    }

    public String getNome() {
        return nome;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public double getMedida() {
        return this.altura;
    }
    
}
