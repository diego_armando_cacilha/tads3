/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProgramacaoIII.Interfaces.Exercicio2;

/**
 *
 * @author aluno
 */
public class Principal {
    public static void main(String[] args) {
        Pessoa pes = new Pessoa(1.76, "Diego");
        Pessoa pes2 = new Pessoa(1.66, "Diego1");
        Pessoa pes3 = new Pessoa(1.20, "Diego2");
        Pessoa pes4 = new Pessoa(1.50, "Diego3");
        
        DataSet dat = new DataSet();
        
        dat.adiciona(pes2);
        dat.adiciona(pes);
        dat.adiciona(pes3);
        dat.adiciona(pes4);
        
        System.out.println(dat.getMaximo().getMedida());
        System.out.println(dat.getMedia());
        System.out.println(dat.getMinimo().getMedida());
        
    }
    
}
