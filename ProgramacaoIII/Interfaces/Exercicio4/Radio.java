/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProgramacaoIII.Interfaces.Exercicio4;

/**
 *
 * @author diego.cacilha
 */
public interface Radio {
    public void setEmissora();
    public double getEmissora();
    public String getTipoEmissora();
    public void setVolumeRadio();
    public int getVolumeRadio();
}
