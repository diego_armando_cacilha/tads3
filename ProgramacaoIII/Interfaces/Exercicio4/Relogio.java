/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProgramacaoIII.Interfaces.Exercicio4;

/**
 *
 * @author diego.cacilha
 */
public interface Relogio {
    public void setHorario();
    public double getHorario();
    public void setHorarioAlarme();
    public double getHorarioAlarme();
    public void ligarAlarme();
    public void desligarAlarme();
    public void setVolumeRelogio();
    public int getVolumeRelogio();
}
