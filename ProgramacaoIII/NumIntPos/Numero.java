/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NumIntPos;

/**
 *
 * @author Diego
 */
public class Numero {
    private int x;
    
    public Numero(int x)
    {
        this.x = x;
    }
    public int multiplica(int y){
        int result = this.x * y;
        return result;
    }
    
    public void getFatorial()
    {
        int aux1 = this.x;
        int aux2 = this.x-1;
        while(aux2 > 0)
        {
            aux1 = aux1 * (aux2);
            aux2--;
        }
        System.out.println("Fatorial de x é "+aux1);
    }
    
    public int getX() {
        return x;
    }

    public void setX(int x) {
        if(x >= 0)
        {
            this.x = x;
        }else
        {
            System.out.println("O número informado é menor que zero.");
        }
        
    }
    
    
}
