/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Televisao;

/**
 *
 * @author Diego
 */
public class ControleRemoto {
    
    private Televisao tel = new Televisao();
    
    public void aumentaVol()
    {
        
        if(tel.getVolAtual() == tel.getVolume())
        {
            System.out.println("Volume máximo");
        }else
        {
            int x = tel.getVolAtual();
            x++;
            tel.setVolAtual(x);
        }
        
    }
    public void diminuiVol()
    {
        if(tel.getVolAtual() == 0)
        {
            System.out.println("Mudo");
        }else
        {
            int x = tel.getVolAtual();
            x--;
            tel.setVolAtual(x);
        }
        
    }
    
    public void proximoCan()
    {
        if(tel.getCanal() == tel.getCanAtual())
        {
            tel.setCanAtual(1);
        }else
        {
            int x = tel.getCanAtual();
            x++;
            tel.setCanAtual(x);
        }
    }
    
    public void voltaCan()
    {
        if(tel.getCanAtual() == 1){
            tel.setCanAtual(tel.getCanal());
        }else
        {
            int x = tel.getCanAtual();
            x--;
            tel.setCanAtual(x);
        }
    }
    
    public String retVol()
    {
        return ("O volume atual é "+tel.getVolAtual());
    }
    public String retCan()
    {
        return ("O canal atual é "+tel.getCanAtual());
    }
    
}
