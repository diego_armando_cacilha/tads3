/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Televisao;

/**
 *
 * @author Diego
 */
public class Televisao {
    private int canal = 140;
    private int volume = 50;
    private int volAtual;
    private int canAtual;
    
    

    public int getCanAtual() {
        return canAtual;
    }

    public int getCanal() {
        return canal;
    }

    public int getVolAtual() {
        return volAtual;
    }

    public int getVolume() {
        return volume;
    }

    public void setCanAtual(int canAtual) {
        this.canAtual = canAtual;
    }

    public void setCanal(int canal) {
        this.canal = canal;
    }

    public void setVolAtual(int volAtual) {
        this.volAtual = volAtual;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }
    
}
