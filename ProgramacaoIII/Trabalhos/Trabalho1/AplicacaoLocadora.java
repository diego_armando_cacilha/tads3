package ProgramacaoIII.Trabalhos.Trabalho1;

import java.util.Scanner;

/**
 *
 * @author diego.cacilha
 */
public class AplicacaoLocadora {
    public static void main(String[] args) {
        Locadora locadora = new Locadora(10, 10);
        int opcao = 0;
        
        Scanner ler = new Scanner(System.in);
        
        while (opcao != 8){
            System.out.println("Opções:");
            System.out.println("1 - Cadastrar Veículo");
            System.out.println("2 - Cadastrar Cliente");
            System.out.println("3 - Alugar Carro");
            System.out.println("4 - Devolver Veículo");
            System.out.println("5 - Fazer Pagamento");
            System.out.println("6 - Listar Veículos Alugados");
            System.out.println("7 - Listar Clientes Devedores");
            System.out.println("8 - Sair");
            System.out.println("Selecione uma opção");
            opcao = ler.nextInt();
            switch(opcao){
                case 1:{
                    System.out.println("Cadastrar Moto ou Carro (0 - moto, 1 carro)?");
                    opcao = ler.nextInt();
                    if(opcao == 0){//Professor, não sei pq não funciona com objeto tipo moto
                        System.out.println("Placa da Moto:");
                        String tmp = ler.next();
                        System.out.println("Cilindradas:");
                        opcao = ler.nextInt();
                        Moto tmpMoto = new Moto(tmp, opcao);
                        locadora.cadastrarVeiculo(tmpMoto);
                    }else if(opcao == 1){
                        System.out.println("Placa do Carro:");
                        String tmp = ler.next();
                        System.out.println("Cilindradas:");
                        opcao = ler.nextInt();
                        Carro tmpMoto = new Carro(tmp, opcao);
                        locadora.cadastrarVeiculo(tmpMoto);
                    }else
                        System.out.println("Opção inválida!");
                    break;
                }
                case 2:{
                    String nome;
                    String cpf;
                    String rua;
                    int numero;
                    
                    System.out.println("Nome:");
                    ler.nextLine();//esvazia o buffer
                    nome = ler.nextLine();
                    
                    System.out.println("Cpf:");
                    cpf = ler.next();
                    
                    System.out.println("Rua:");
                    ler.nextLine();//esvazia o buffer
                    rua = ler.nextLine();
                    
                    System.out.println("Número:");
                    numero = ler.nextInt();
                    
                    Endereco endereco = new Endereco(rua, numero);
                    Cliente tmpCli = new Cliente(nome, cpf, endereco);
                    
                    locadora.cadastrarCliente(tmpCli);
                    
                    break;
                }
                case 3:{
                    String placa;
                    String cpf;
                    System.out.println("Informe a placa do veículo:");
                    placa = ler.next();
                    System.out.println("Informe o CPF do cliente:");
                    cpf = ler.next();
                    
                    locadora.alugarVeiculo(placa, cpf);
                    
                    System.out.println("Cadastro de aluguel realizado com sucesso!");
                    break;
                }
                case 4:{
                    System.out.println("Informa a placa do veículo:");
                    String placa = ler.next();
                    
                    locadora.devolverVeiculo(placa);
                    
                    System.out.println("Veículo devolvido com sucesso!");
                    break;
                }
                case 5:{
                    System.out.println("Informe o CPF do cliente:");
                    String cpf = ler.next();
                    System.out.println("Valor a pagar:");
                    int valor = ler.nextInt();
                    
                    locadora.fazerPagamento(cpf, valor);
                    
                    System.out.println("Valor pago com sucesso!");                    
                    break;
                }
                case 6:{
                    System.out.println(locadora.listarVeiculosAlugados().toString());
                    
                    break;
                }
                case 7:{
                    System.out.println(locadora.listarClientesDevedores().toString());
                    
                    break;
                }
                case 8:{
                    break;  
                }
                default:{
                    System.out.println("A opção não existe");
                    break;
                }
            }
        }
    }
    
    
    
}
