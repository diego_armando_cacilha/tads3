package ProgramacaoIII.Trabalhos.Trabalho1;

public class Carro extends Veiculo{
    private int cilindradas;

    public Carro(String placa, int cilindradas) {
        super(placa);
        this.cilindradas = cilindradas;
        setAutonomia(10);
    }
    
    @Override
    public String toString() {
        String ret = "Placa: " + getPlaca() + ", Combustível no tanque: " + getCombustivelNoTanque() + ", quilometragem: " 
                + getQuilometragem() + ", alugado: " + isAlugado() + ", preço diário: " + getPrecoDiaria() 
                + "Cilindrada: " + this.cilindradas + ", placa: " + this.getPlaca();
        return ret;
    }

    public int getCilindradas() {
        return cilindradas;
    }

    public void setCilindradas(int cilindradas) {
        this.cilindradas = cilindradas;
    }
    
}
