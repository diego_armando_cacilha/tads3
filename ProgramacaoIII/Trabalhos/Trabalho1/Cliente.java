package ProgramacaoIII.Trabalhos.Trabalho1;

public class Cliente {
    private String cpf;
    private String nome;
    private Endereco endereco;
    private double valorDaDivida;

    public Cliente(String nome, String cpf, Endereco endereco) {
        this.nome = nome;
        this.cpf = cpf;
        this.endereco = endereco;
        this.valorDaDivida = 0;
    }

    @Override
    public String toString() {
        return "Nome: " + this.nome +". CPF: " + this.cpf;
    }

    public boolean equals(Cliente cliente) {
        return cliente.getCpf() == this.cpf;
    }

    public String getCpf() {
        return cpf;
    }

    public String getNome() {
        return nome;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public double getValorDaDivida() {
        return valorDaDivida;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setValorDaDivida(double valorDaDivida) {
        this.valorDaDivida = valorDaDivida;
    }
    
}
