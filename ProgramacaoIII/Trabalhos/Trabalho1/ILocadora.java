package ProgramacaoIII.Trabalhos.Trabalho1;

public interface ILocadora {
    
    public void cadastrarVeiculo(Veiculo veiculo);
    public void cadastrarCliente(Cliente cliente);
    public Veiculo localizarVeiculo(String placa);
    public Cliente localizarCliente(String cpf);
    public void alugarVeiculo(String placa, String cpf);
    public void devolverVeiculo(String placa);
    public void fazerPagamento(String cpf, double valorAPagar);
    public String listarVeiculosAlugados();
    public String listarClientesDevedores();
    
}
