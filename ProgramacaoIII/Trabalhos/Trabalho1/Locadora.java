package ProgramacaoIII.Trabalhos.Trabalho1;

import java.util.ArrayList;

public class Locadora implements ILocadora{
    private int qtdVeiculos;
    private int qtdClientes;
    private int limite;
//    private ArrayList<Carro> frotaCarros1 = new ArrayList();
//    private ArrayList<Cliente> listaDeClientes1 = new ArrayList();
    private Cliente[] listaDeClientes;
    private Veiculo[] frotaVeiculos;
    private int contVeiculos;
    private int contClientes;

    public Locadora(int qtdCarros, int qtdClientes) {
        listaDeClientes = new Cliente[qtdClientes];
        frotaVeiculos = new Carro[qtdCarros];
        this.contVeiculos = 0;
        this.contClientes = 0;
   }

    @Override
    public void cadastrarVeiculo(Veiculo veiculo) {
        this.frotaVeiculos[this.contVeiculos] = veiculo;
        this.contVeiculos++;
        //setFrotaCarros(frotaCarros);
    }

    @Override
    public void cadastrarCliente(Cliente cliente) {
        listaDeClientes[this.contClientes] = cliente;
        this.contClientes++;
    }

    @Override
    public Veiculo localizarVeiculo(String placa) {
        Veiculo tmp = null;
        for(int i = 0; i < this.contVeiculos; i++){
            if(frotaVeiculos[i].getPlaca().equals(placa)){
                tmp = frotaVeiculos[i];
            }
        }
        return tmp;
    }

    @Override
    public Cliente localizarCliente(String cpf) {
        Cliente tmp = null;
        for(int i = 0; i < this.contClientes; i++){
            if(listaDeClientes[i].getCpf().equals(cpf)){
                tmp = listaDeClientes[i];
            }
        }
        return tmp;
    }

    @Override
    public void alugarVeiculo(String placa, String cpf) {
        localizarVeiculo(placa).setAlugado(true);
        localizarCliente(cpf).setValorDaDivida(localizarVeiculo(placa).getPrecoDiaria());
    }

    /**
     * Pesquisa veículo no array da frota de veículos da locadora.
     * 
     * @param placa Placa do veículo a ser pesquisado
     */
    @Override
    public void devolverVeiculo(String placa) {
        localizarVeiculo(placa).setAlugado(false);
    }

    /**
     * Faz pagamento da dívida do cliente que alugou um carro.
     * 
     * @param cpf CPF da pessoa a ser pesquisada no array de clientes
     * @param valorAPagar Valor que o cliente deve pagar pelo aluguei do veículo
     */
    @Override
    public void fazerPagamento(String cpf, double valorAPagar) {
        localizarCliente(cpf).setValorDaDivida(localizarCliente(cpf).getValorDaDivida() - valorAPagar);
    }
    
    @Override
    public String listarVeiculosAlugados() {
        String tmp = "";
        for(int i = 0; i < contVeiculos; i++){
            if(this.frotaVeiculos[i].isAlugado() == true)
                tmp += this.frotaVeiculos[i].toString();
        }
        return tmp;
    }

    @Override
    public String listarClientesDevedores() {
        String tmp = "";
        for(int i = 0; i < this.contClientes; i++){
            if(this.listaDeClientes[i].getValorDaDivida() > 0)
                tmp += this.listaDeClientes[i].toString();
        }
        
        return tmp;
    }
    
    public Veiculo[] getFrotaCarros() {
        return frotaVeiculos;
    }

    public Cliente[] getListaDeClientes() {
        return listaDeClientes;
    }

    public void setFrotaCarros(Carro[] frotaCarros) {
        this.frotaVeiculos = frotaCarros;
    }

    public void setListaDeClientes(Cliente[] listaDeClientes) {
        this.listaDeClientes = listaDeClientes;
    }
    
    public int getQtdClientes() {
        return qtdClientes;
    }

    public void setQtdClientes(int qtdClientes) {
        this.qtdClientes = qtdClientes;
    }

    public int getLimite() {
        return limite;
    }

    public void setLimite(int limite) {
        this.limite = limite;
    }

    public int getQtdVeiculos() {
        return qtdVeiculos;
    }

    public void setQtdVeiculos(int qtdVeiculos) {
        this.qtdVeiculos = qtdVeiculos;
    }
}
