package ProgramacaoIII.Trabalhos.Trabalho1;

public class Moto extends Veiculo{
    private int cilindradas;
    
    public Moto(String placa, int cilindradas) {
        super(placa);
        this.cilindradas = cilindradas;
        setAutonomia(30);
    }
    
    @Override
    public String toString() {
        String ret = "Placa: " + getPlaca() + ", Combustível no tanque: " + getCombustivelNoTanque() + ", quilometragem: " 
                + getQuilometragem() + ", alugado: " + isAlugado() + ", preço diário: " + getPrecoDiaria() 
                + "Cilindrada: " + this.cilindradas + ", placa: " + this.getPlaca();
        return ret;
    }

    public int getCilindradas() {   
        return cilindradas;
    }

    public void setCilindradas(int cilindradas) {
        this.cilindradas = cilindradas;
    }
}
