/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProgramacaoIII.Trabalhos.Trabalho1;

/**
 * 
 * @author diego.cacilha
 */
abstract public class Veiculo {
    
    private String placa;
    private double combustivelNoTanque;
    private int quilometragem;
    private boolean alugado;
    private double precoDiaria;
    private int autonomia; //recebe a quantidade de KM que o veículo faz com um litro de combustível

    public Veiculo(String placa) {
        this.placa = placa;
        this.combustivelNoTanque = 0;
        this.quilometragem = 0;
        this.alugado = false;
    }
    
    public void abastecer(int combustivel){
        this.combustivelNoTanque = combustivel;
    }
    
    public void viajar(int distancia) throws Exception{
        double aux = this.combustivelNoTanque * this.autonomia;
        double gastou = distancia / this.autonomia;
        if(aux >= distancia){
            this.combustivelNoTanque = this.combustivelNoTanque - gastou;
        }else{
            throw new Exception("Quantidade de Combustível é insuficiente para percorer a distância informada");
        }
    }
    
    @Override
    public String toString(){
        String ret = "Placa: "+this.placa + ", Combustível no tanque: " + this.combustivelNoTanque + ", quilometragem: " 
                + this.quilometragem + ", alugado: " + this.alugado + ", preço diário: " + this.precoDiaria;
        return ret;
    }
    
    public boolean equals(Veiculo veiculo){
        return veiculo.placa == this.placa;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public double getCombustivelNoTanque() {
        return combustivelNoTanque;
    }

    public void setCombustivelNoTanque(int combustivelNoTanque) {
        this.combustivelNoTanque = combustivelNoTanque;
    }

    public int getQuilometragem() {
        return quilometragem;
    }

    public void setQuilometragem(int quilometragem) {
        this.quilometragem = quilometragem;
    }

    public boolean isAlugado() {
        return alugado;
    }

    public void setAlugado(boolean alugado) {
        this.alugado = alugado;
    }

    public double getPrecoDiaria() {
        return precoDiaria;
    }

    public void setPrecoDiaria(double precoDiaria) {
        this.precoDiaria = precoDiaria;
    }

    public void setAutonomia(int autonomia) {
        this.autonomia = autonomia;
    }

    public int getAutonomia() {
        return autonomia;
    }
    
}
