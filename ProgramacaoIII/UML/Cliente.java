/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProgramacaoIII.UML;

/**
 *
 * @author aluno
 */
public class Cliente {
    private String nome, cpf;
    private boolean sexo;
    private ContaBancaria conta;
    
    public Cliente(String nome, String cpf, boolean sexo)
    {
        this.nome = nome; 
        this.cpf = cpf;
        this.sexo = sexo;
    }
}
