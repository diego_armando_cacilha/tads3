/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProgramacaoIII.UML;

/**
 *
 * @author guest-j4IfKN
 */
public class ContaBancaria{
    private int numero;
    private String nomeCliente;
    private double saldo;
    private double limiteCredito = 100;
    private Cliente titular;
        
    public boolean saca(double boo){
        return true;
    }
    
    public void deposita(double valor){
        saldo += valor;
        System.out.println("Depósito efetuado com sucesso.");
    }

    public double getLimiteCredito() {
        return limiteCredito;
    }

    public String getNomeCliente() {
        return nomeCliente;
    }

    public int getNumero() {
        return numero;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setLimiteCredito(double limiteCredito) {
        this.limiteCredito = limiteCredito;
    }

    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }
    
}
