/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProgramacaoIII.UML;

/**
 *
 * @author guest-j4IfKN
 */
public class Main {
    public static void main(String[] args) {
        ContaBancaria contaJunior = new ContaBancaria();
        contaJunior.setNumero(1143166);
        contaJunior.setNomeCliente("Diego Armando Cailha");
        contaJunior.setSaldo(100.0);
        contaJunior.setLimiteCredito(10.0);
        contaJunior.deposita(10);
        
        ContaBancaria conta2 = contaJunior;
        
        conta2.deposita(10);
        
        //System.out.println(contaJunior.getSaldo());
        System.out.println(conta2.equals(contaJunior));
        
    }
}
